<?php
/**
 * Redux Theme functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package modis
 */

if ( ! class_exists( 'ReduxFramewrk' ) ) {
    require_once( get_template_directory() . '/framework/sample-config.php' );
	function removeDemoModeLink() { // Be sure to rename this function to something more unique
		if ( class_exists('ReduxFrameworkPlugin') ) {
			remove_filter( 'plugin_row_meta', array( ReduxFrameworkPlugin::get_instance(), 'plugin_metalinks'), null, 2 );
		}
		if ( class_exists('ReduxFrameworkPlugin') ) {
			remove_action('admin_notices', array( ReduxFrameworkPlugin::get_instance(), 'admin_notices' ) );    
		}
	}
	add_action('init', 'removeDemoModeLink');
}

if ( ! function_exists( 'modis_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function modis_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Redux Theme, use a find and replace
	 * to change 'modis' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'modis', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'custom-header' );
	add_theme_support( 'custom-background' ); 

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', 'modis' ),
		'left'	=> esc_html__( 'Left Menu', 'modis' ),
		'right'	=> esc_html__( 'Right Menu', 'modis' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-list',
		'comment-form',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'audio',
		'image',
		'video',
		'gallery',
	) );
	add_image_size( 'career-thumb', 70, 82, array( 'left', 'top' ) );
	
}
endif; // modis_setup
add_action( 'after_setup_theme', 'modis_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function modis_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'modis_content_width', 640 );
}
add_action( 'after_setup_theme', 'modis_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function modis_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'modis' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Appears in the sidebar section of the site.', 'modis' ),  
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Shop Sidebar', 'modis' ),
		'id'            => 'shop-sidebar',
		'description'   => esc_html__( 'Appears in the sidebar section of the site.', 'modis' ),  
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer One Widget Area', 'modis' ),
		'id'            => 'footer-area-1',
		'description'   => esc_html__( 'Footer Widget that appears on the Footer.', 'modis' ),
		'before_widget' => '<div id="%1$s" class="widget footer-widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	) );
	
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Two Widget Area', 'modis' ),
		'id'            => 'footer-area-2',
		'description'   => esc_html__( 'Footer Widget that appears on the Footer.', 'modis' ),
		'before_widget' => '<div id="%1$s" class="widget footer-widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	) );
	
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Three Widget Area', 'modis' ),
		'id'            => 'footer-area-3',
		'description'   => esc_html__( 'Footer Widget that appears on the Footer.', 'modis' ),
		'before_widget' => '<div id="%1$s" class="widget footer-widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	) );
	
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Fourth Widget Area', 'modis' ),
		'id'            => 'footer-area-4',
		'description'   => esc_html__( 'Footer Widget that appears on the Footer.', 'modis' ),
		'before_widget' => '<div id="%1$s" class="widget footer-widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	) ); 

}
add_action( 'widgets_init', 'modis_widgets_init' );

/**
 * Enqueue Google fonts.
 */
function modis_fonts_url() {
    $fonts_url = '';

    /* Translators: If there are characters in your language that are not
    * supported by Lora, translate this to 'off'. Do not translate
    * into your own language.
    */
    $roboto_condensed = _x( 'on', 'Roboto Condensed font: on or off', 'modis' );
 
    /* Translators: If there are characters in your language that are not
    * supported by Lora, translate this to 'off'. Do not translate
    * into your own language.
    */
    $open_sans = _x( 'on', 'Open Sans font: on or off', 'modis' );
 
    /* Translators: If there are characters in your language that are not
    * supported by Open Sans, translate this to 'off'. Do not translate
    * into your own language.
    */
    $playfair_display = _x( 'on', 'Playfair Display font: on or off', 'modis' );    

    /* Translators: If there are characters in your language that are not
    * supported by Open Sans, translate this to 'off'. Do not translate
    * into your own language.
    */
    $montserrat = _x( 'on', 'Montserrat font: on or off', 'modis' );     
 
    if ( 'off' !== $roboto_condensed || 'off' !== $open_sans || 'off' !== $playfair_display || 'off' !==$montserrat ) {
        $font_families = array();

        if ( 'off' !== $roboto_condensed ) {
            $font_families[] = 'Roboto Condensed:400,300,300italic,400italic,700,700italic';
        }        
 
        if ( 'off' !== $open_sans ) {
            $font_families[] = 'Open Sans:300,500,600,700,900,400';
        }
 
        if ( 'off' !== $playfair_display ) {
            $font_families[] = 'Playfair Display:400,400italic,700,700italic';
        } 
        if ( 'off' !== $montserrat ) {
            $font_families[] = 'Montserrat:400,700';
        } 
 		
        $query_args = array(
            'family' => urlencode( implode( '|', $font_families ) ),
            'subset' => urlencode( 'latin,latin-ext' ),
        );
 
        $fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
    }
 
    return esc_url_raw( $fonts_url );
}


/**
 * Enqueue scripts and styles.
 */
function modis_scripts() {
	global $modis_option;
	$protocol = is_ssl() ? 'https' : 'http';
	$gmapaipkey = $modis_option['gmap_apikey'];

	// Add custom fonts, used in the main stylesheet.
    wp_enqueue_style( 'modis-fonts', modis_fonts_url(), array(), null );

    /** All frontend css files **/  
    if ($gmapaipkey != '') {
		wp_enqueue_script( "modis-mapapi", "$protocol://maps.googleapis.com/maps/api/js?key=$gmapaipkey",array('jquery'),false,false );
	}  
    wp_enqueue_style( 'bootstrap', get_template_directory_uri().'/css/bootstrap.css');

    if($modis_option['preload-switch']!=false){
        if(isset($modis_option['preloader_mode']) and $modis_option['preloader_mode']=="preloader_progress" ){
    	    wp_enqueue_style( 'modis-jpreloader', get_template_directory_uri().'/css/jpreloader.css');
        }else{
            wp_enqueue_style( 'modis-jpreloader', get_template_directory_uri().'/css/royal-preloader.css');
        }
    }

	if($modis_option['animate-switch']!=false){
		wp_enqueue_style( 'modis-animate', get_template_directory_uri().'/css/animate.css'); 
	}
	wp_enqueue_style( 'modis-plugin', get_template_directory_uri().'/css/plugin.css');
	wp_enqueue_style( 'owl-carousel', get_template_directory_uri().'/css/owl.carousel.css');
	wp_enqueue_style( 'owl-theme', get_template_directory_uri().'/css/owl.theme.css');
	wp_enqueue_style( 'transitions', get_template_directory_uri().'/css/owl.transitions.css');
	wp_enqueue_style( 'magnific', get_template_directory_uri().'/css/magnific-popup.css');
	wp_enqueue_style( 'modis-bg', get_template_directory_uri().'/css/bg.css');
	wp_enqueue_style( 'font-awesome', get_template_directory_uri().'/fonts/font-awesome/css/font-awesome.css');
	wp_enqueue_style( 'font-elegant', get_template_directory_uri().'/fonts/elegant_font/HTML_CSS/style.css');
	wp_enqueue_style( 'font-et-line', get_template_directory_uri().'/fonts/et-line-font/style.css');
	wp_enqueue_style( 'font-elegant-ie', get_template_directory_uri().'/fonts/elegant_font/HTML_CSS/lte-ie7.js');	
	wp_enqueue_style( 'woo', get_template_directory_uri().'/css/woocommerce.css');
	wp_enqueue_style( 'datepicker', get_template_directory_uri().'/css/datepicker.css');
	wp_enqueue_style( 'modis-style', get_stylesheet_uri() );
	if($modis_option['theme_version']=="ver2"){
		wp_enqueue_style( 'modis-barber', get_template_directory_uri().'/css/custom-barber.css'); 
	}
	
	/** All frontend js files **/    
	if($modis_option['preload-switch']==true){
        if(isset($modis_option['preloader_mode']) and $modis_option['preloader_mode']=="preloader_progress" ){
            wp_enqueue_script("modis-jpreLoader", get_template_directory_uri()."/js/jpreLoader.js",array('jquery'),false,true);
        }else{
            wp_enqueue_script("modis-preloader", get_template_directory_uri()."/js/royal_preloader.min.js",array('jquery'),false,false); 
        }
    }

	wp_enqueue_script("bootstrap", get_template_directory_uri()."/js/bootstrap.min.js",array('jquery'),false,true);

	wp_enqueue_script("isotope", get_template_directory_uri()."/js/isotope.pkgd.js",array('jquery'),false,true);
    wp_enqueue_script("easing", get_template_directory_uri()."/js/easing.js",array('jquery'),false,true);
	wp_enqueue_script("flexslider", get_template_directory_uri()."/js/jquery.flexslider-min.js",array('jquery'),false,true);
    wp_enqueue_script("scrollto", get_template_directory_uri()."/js/jquery.scrollto.js",array('jquery'),false,true);
    wp_enqueue_script("carousel", get_template_directory_uri()."/js/owl.carousel.js",array('jquery'),false,true);
	wp_enqueue_script("countTo", get_template_directory_uri()."/js/jquery.countTo.js",array('jquery'),false,true);
	wp_enqueue_script("classie", get_template_directory_uri()."/js/classie.js",array('jquery'),false,true);
	wp_enqueue_script("modis-video-resize", get_template_directory_uri()."/js/video.resize.js",array('jquery'),false,true);
	wp_enqueue_script("fitvids", get_template_directory_uri()."/js/jquery.fitvids.js",array('jquery'),false,true);
	if($modis_option['animate-switch']!=false){
		wp_enqueue_script("wow", get_template_directory_uri()."/js/wow.min.js",array('jquery'),false,true);
	}
	wp_enqueue_script("magnific", get_template_directory_uri()."/js/jquery.magnific-popup.min.js",array('jquery'),false,true);
	wp_enqueue_script("enquire", get_template_directory_uri()."/js/enquire.min.js",array('jquery'),false,true);
	wp_enqueue_script("stellar", get_template_directory_uri()."/js/jquery.stellar.min.js",array('jquery'),false,true);
	wp_enqueue_script("typed", get_template_directory_uri()."/js/typed.js",array('jquery'),false,true);
	wp_enqueue_script("datepicker", get_template_directory_uri()."/js/bootstrap-datepicker.js",array('jquery'),false,true);
	wp_enqueue_script("modis-custom", get_template_directory_uri()."/js/designesia.js",array('jquery'),false,true);

}
add_action( 'wp_enqueue_scripts', 'modis_scripts' );

if(!function_exists('modis_custom_frontend_style')){
    function modis_custom_frontend_style(){
        global $modis_option;
        echo '<style type="text/css">'.$modis_option['custom-css'].'</style>';
    }
}
add_action('wp_head', 'modis_custom_frontend_style');

/**
 * Implement the Custom Meta Boxs.
 */
require get_template_directory() . '/framework/meta-boxes.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/framework/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/framework/widget/flickr.php';
/**
 * Custom shortcode plugin visual composer.
 */
require_once get_template_directory() . '/shortcodes.php';
require_once get_template_directory() . '/vc_shortcode.php';
/**
 * Customizer Menu.
 */
require get_template_directory() . '/framework/wp_bootstrap_navwalker.php';
/**
 * Customizer additions.
 */
require get_template_directory() . '/framework/customizer.php';

//Code Visual Composer.
// Add new Param in Row
if(function_exists('vc_add_param')){
	vc_add_param(
		'vc_row',
		array(
			"type" => "dropdown",
			"heading" => esc_html__('Setup Full width For Row', 'modis'),
			"param_name" => "fullwidth",
			"value" => array(   
			                esc_html__('No', 'modis') => 'no',  
			                esc_html__('Yes', 'modis') => 'yes',                                                                                
			              ),
			"description" => esc_html__("Select Full width for row : yes or not, Default: No fullwidth", "modis"),      
        )
    );
    vc_add_param(
		'vc_row',
		array(
			'type' => 'checkbox',
			'heading' => esc_html__( 'Full height row?', 'modis' ),
			'param_name' => 'fullheight',
			'description' => esc_html__( 'If checked row will be set to full height.', 'modis' ),
			'value' => array( esc_html__( 'Yes', 'modis' ) => 'yes' ),
		)
    ); 
    vc_add_param(
		'vc_row',
		array(
			'type' => 'checkbox',
			'heading' => esc_html__( 'Parallax background for row?', 'modis' ),
			'param_name' => 'bg_parallax_section',
			'description' => esc_html__( 'Add parallax type background for row (Note: If no image is specified, parallax will use background image from Design Options).', 'modis' ),
			'value' => array( esc_html__( 'Yes', 'modis' ) => 'yes' ),
		)
    );
    vc_add_param(
		'vc_row',
    	array(
			'type' => 'attach_image',
			'heading' => esc_html__( 'Image', 'modis' ),
			'param_name' => 'parallax_image_section',
			'value' => '',
			'description' => esc_html__( 'Select image from media library.', 'modis' ),
			'dependency' => array(
				'element' => 'bg_parallax_section',
				'not_empty' => true,
			),
		)
    );
    vc_add_param(
    	'vc_row',
    	array(
		  "type" => "textfield",
		  "heading" => esc_html__('Parallax speed', 'modis'),
		  "param_name" => "parallax_speed_section",
		  "value" => ".5",
		  "description" => esc_html__("Enter parallax speed ratio (Note: Default value is .5, min value is .5)", "modis"), 
		  'dependency' => array(
				'element' => 'bg_parallax_section',
				'not_empty' => true,
			),
		) 
    );
    
	vc_add_param(
		'vc_row',
		array(
			'type' => 'checkbox',
			'heading' => esc_html__( 'Put a pictures on the half Row?', 'modis' ),
			'param_name' => 'img_halfrow',
			'description' => esc_html__( 'If checked, then start setup image on half row.', 'modis' ),
			'value' => array( esc_html__( 'Yes', 'modis' ) => 'yes' ),
		)
    );
    vc_add_param(
		'vc_row',
    	array(
			'type' => 'attach_image',
			'heading' => esc_html__( 'Image', 'modis' ),
			'param_name' => 'halfrow_image',
			'value' => '',
			'description' => esc_html__( 'Select image from media library.', 'modis' ),
			'dependency' => array(
				'element' => 'img_halfrow',
				'not_empty' => true,
			),
		)
    );
  //   vc_add_param(
		// 'vc_row',
  //   	array(
		// 	"type" => "colorpicker",
		// 	"heading" => esc_html__("Background color", 'modis' ),
		// 	"param_name" => "bgcolor",
		// 	"value" => "",
		// 	"description" => esc_html__("Add background color.", 'modis' ),
		// 	'dependency' => array(
		// 		'element' => 'img_halfrow',
		// 		'not_empty' => true,
		// 	),
  //     	)
  //   );
	vc_add_param(
		'vc_row',
		array(
			'type' => 'dropdown',
			'heading' => esc_html__( 'Columns image', 'modis' ),
			'param_name' => 'img_columns',
			'value' => array(
				esc_html__( 'Default', 'modis' ) => '',
				esc_html__( 'Image on 4 Columns', 'modis' ) => '4columns',
				esc_html__( 'Image on 5 Columns', 'modis' ) => '5columns',
				esc_html__( 'Image on 6 Columns', 'modis' ) => '6columns',
				esc_html__( 'Image on 7 Columns', 'modis' ) => '7columns',
			),
			'description' => esc_html__( 'Select columns position within row.', 'modis' ),
			'dependency' => array(
				'element' => 'img_halfrow',
				'not_empty' => true,
			),
		)
    );
    vc_add_param(
		'vc_row',
		array(
			'type' => 'dropdown',
			'heading' => esc_html__( 'Image position', 'modis' ),
			'param_name' => 'img_position',
			'value' => array(
				esc_html__( 'Default', 'modis' ) => '',
				esc_html__( 'Image on Left Row', 'modis' ) => 'imgleft',
				esc_html__( 'Image on Right Row', 'modis' ) => 'imgright',				
			),
			'description' => esc_html__( 'Select Image position within row.', 'modis' ),
			'dependency' => array(
				'element' => 'img_halfrow',
				'not_empty' => true,
			),
		)
    ); 

    // Add new Param in Column	
	vc_add_param('vc_column',array(
		  "type" => "dropdown",
		  "heading" => esc_html__('Animate Column', 'modis'),
		  "param_name" => "animate",
		  "value" => array(   
							esc_html__('None', 'modis') => 'none', 
							esc_html__('Fade In Down', 'modis') => 'fadeindown', 
							esc_html__('Fade In', 'modis') => 'fadein', 
							esc_html__('Fade In Left', 'modis') => 'fadeinleft',  
							esc_html__('Fade In Right', 'modis') => 'fadeinright',
							esc_html__('Fade In Up', 'modis') => 'fadeinup',
							esc_html__('Slide In Left', 'modis') => 'slideinleft',
							esc_html__('Slide In Right', 'modis') => 'slideinright',
							esc_html__('Slide In Down', 'modis') => 'slideindown',
							esc_html__('Slide In Up', 'modis') => 'slideinup',
						  ),
		  "description" => esc_html__("Select Animate , Default: None", "modis"),      
		) 
    );

	vc_add_param('vc_column',array(
		  "type" => "textfield",
		  "heading" => esc_html__('Animate delay number.', 'modis'),
		  "param_name" => "delay",
		  "value" => "",
		  "description" => esc_html__("Example : 0.2, 0.6, 1, etc", "modis"), 
		  "dependency"  => array( 'element' => 'animate', 'value' => array( 'fadeinup', 'fadeindown', 'fadein', 'fadeinleft', 'fadeinright' ) ),     
		) 
    );     
}

	

if(function_exists('vc_remove_param')){
	vc_remove_param( "vc_row", "parallax" );
	vc_remove_param( "vc_row", "parallax_image" );
	vc_remove_param( "vc_row", "full_width" );
	vc_remove_param( "vc_row", "full_height" );
	vc_remove_param( "vc_row", "video_bg" );
	vc_remove_param( "vc_row", "video_bg_parallax" );
	vc_remove_param( "vc_row", "video_bg_url" );
	vc_remove_param( "vc_row", "columns_placement" );
	vc_remove_param( "vc_row", "gap" );
	vc_remove_param( "vc_row", "parallax_speed_video" );
	vc_remove_param( "vc_row", "parallax_speed_bg" );
}	

/**
 * Require plugins install for this theme.
 *
 * @since Split Vcard 1.0
 */
require_once get_template_directory() . '/framework/plugin-requires.php';



