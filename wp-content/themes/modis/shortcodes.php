<?php 
// OT Heading
add_shortcode('heading','heading_func');
function heading_func($atts, $content = null){
	extract(shortcode_atts(array(
		'title'		=>	'',
		'align'		=>	'left',
		'line'		=>	'',
        'el_class'  =>  '',
        'tag'       =>  'h1',
	), $atts));

	ob_start(); 
?>
	
	<div class="<?php if($align=='center'){echo 'text-center';}elseif($align=='right'){echo 'text-right';}else{echo 'text-left';} ?> <?php echo esc_attr($el_class); ?>">
		<<?php echo esc_attr($tag); ?>><?php echo htmlspecialchars_decode($title); ?></<?php echo esc_attr($tag); ?>>
	    <?php if($line==true){ ?><div class="small-border <?php if($align=='left'){echo 'left';}elseif($align=='right'){echo 'right';} ?>"></div><?php } ?>
	</div>

<?php
    return ob_get_clean();
}

// Button
add_shortcode('button','button_func');
function button_func($atts, $content = null){
    extract(shortcode_atts(array(       
        'text'     =>  '',
        'link'     =>  '',
        'style'    =>  'style1',
        'size'     =>  'normal',
    ), $atts));

    ob_start(); 
?>
    
    <a href="<?php echo esc_url($link); ?>" class="btn <?php if($style=='style1'){echo 'btn-line-white';}elseif($style=='style3'){echo 'btn-bg-color bg-color';}else{echo 'btn-main-color';} ?> <?php if($size=='big'){echo 'btn-big';} ?>"><?php echo esc_attr($text); ?></a>

<?php
    return ob_get_clean();
}

// OT About
add_shortcode('about','about_func');
function about_func($atts, $content = null){
	extract(shortcode_atts(array(		
		'title'		=>	'',
	), $atts));

	ob_start(); 
?>
	
	<div class="padding40">
        <h2><?php echo htmlspecialchars_decode($title); ?>
            <span class="small-border"></span>
        </h2>
        <?php echo htmlspecialchars_decode($content); ?>
    </div>

<?php
    return ob_get_clean();
}

// OT Team
add_shortcode('team','team_func');
function team_func($atts, $content = null){
	extract(shortcode_atts(array(
		'photo'		=>	'',
        'use'       =>  '',
		'name'		=>	'',
		'con1'		=>	'',
		'url1'		=>	'',
		'con2'		=>	'',
		'url2'		=>	'',
		'con3'		=>	'',
		'url3'		=>	'',
		'con4'		=>	'',
		'url4'		=>	'',
	), $atts));
		$img = wp_get_attachment_image_src($photo,'full');
		$img = $img[0];
	ob_start(); 
?>
	
	<div class="team-member">
        <img src="<?php echo esc_url($img); ?>" class="img-circle <?php if($use=='true'){echo 'pic-grey';} ?>" alt="" />
        <h3><?php echo htmlspecialchars_decode($name); ?></h3>
        <p><?php echo htmlspecialchars_decode($content); ?></p>
        <div class="social">
            <?php if($con1){ ?><a href="<?php echo esc_url($url1); ?>"><i class="<?php echo esc_attr($con1); ?> fa-lg"></i></a><?php } ?>
            <?php if($con2){ ?><a href="<?php echo esc_url($url2); ?>"><i class="<?php echo esc_attr($con2); ?> fa-lg"></i></a><?php } ?>
            <?php if($con3){ ?><a href="<?php echo esc_url($url3); ?>"><i class="<?php echo esc_attr($con3); ?> fa-lg"></i></a><?php } ?>
            <?php if($con4){ ?><a href="<?php echo esc_url($url4); ?>"><i class="<?php echo esc_attr($con4); ?> fa-lg"></i></a><?php } ?>
        </div>
    </div>

<?php
    return ob_get_clean();
}



// OT Pricing Table
add_shortcode('pricing','pricing_func');
function pricing_func($atts, $content = null){
	extract(shortcode_atts(array(
		'title'		=>	'',
		'status'	=>	'',
		'price'		=>	'',
		'unit'		=>	'',
		'linkbox'	=>	'',
		'sale'		=>	'',
	), $atts));
		$url 	= vc_build_link( $linkbox );
	ob_start(); 
?>
	
	<div class="table package text-center">
        <div class="c1">
            <h2><?php echo htmlspecialchars_decode($title); ?></h2>
            <?php echo htmlspecialchars_decode($content); ?>
        </div>

        <div class="c2">
            <?php if($status){ ?><strong><?php echo htmlspecialchars_decode($status) ?></strong><?php } ?>
            <h3 class="price"><span><?php echo esc_attr($unit); ?></span><?php echo esc_attr($price); ?></h3>
            <?php if($sale){echo htmlspecialchars_decode($sale);}  ?>
        </div>

        <div class="c3">
            <?php if ( strlen( $linkbox ) > 0 && strlen( $url['url'] ) > 0 ) {
				echo '<a class="btn-line" href="' . esc_attr( $url['url'] ) . '" target="' . ( strlen( $url['target'] ) > 0 ? esc_attr( $url['target'] ) : '_self' ) . '">' . esc_attr( $url['title'] ).'</a>';
			} ?>
        </div>

    </div>

<?php
    return ob_get_clean();
}

// OT Our Skill
add_shortcode('skill','skill_func');
function skill_func($atts, $content = null){
	extract(shortcode_atts(array(
		'title'		=>	'',
		'number'	=>	'',		
	), $atts));

	ob_start(); 
?>
	
	<h3><?php echo htmlspecialchars_decode($title); ?></h3>
    <div class="de-progress">
        <div class="progress-bar" data-value="<?php echo esc_attr($number); ?>%"></div>
    </div>

<?php
    return ob_get_clean();
}

// Google Map
add_shortcode('ggmap','ggmap_func');
function ggmap_func($atts, $content = null){
    extract( shortcode_atts( array(
	  'height'		=> '',	
      'lat'   		=> '',
      'long'	  	=> '',
      'zoom'		=> '15',
      'address'		=> '',
	  'mapcolor'	=> '',
	  'icon'		=> '',
	  'style'		=> 'light',
   ), $atts ) );
   
   $icon1 = wp_get_attachment_image_src($icon,'full');
   $icon1 = $icon1[0];
   $i=rand(0,99999);
   		
    ob_start(); ?>
    	 
    <div id="map<?php echo $i; ?>" style="<?php if($height) echo 'height: '.$height.'px;'; ?>"></div>
	
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAvpnlHRidMIU374bKM5-sx8ruc01OvDjI"></script>
    <script type="text/javascript">	
	(function($) {
    "use strict"// When the window has finished loading create our google map below
                    google.maps.event.addDomListener(window, 'load', init);

                    function init() {
                        // Basic options for a simple Google Map
                        // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
                        var myLatlng = new google.maps.LatLng(<?php echo esc_js( $lat );?>, <?php echo esc_js( $long );?>);

                        var mapOptions = {
                            // How zoomed in you want the map to start at (always required)
                            zoom: <?php echo esc_js($zoom); ?>,
                            disableDefaultUI: true,
							scrollwheel: false, 

                            // The latitude and longitude to center the map (always required)

                            center: myLatlng, // New York

                            // How you would like to style the map. 
                            // This is where you would paste any style found on Snazzy Maps.
                            styles: [<?php if ($style == 'light'){
                              echo  '{
                                    "featureType": "administrative",
                                    "elementType": "labels.text.fill",
                                    "stylers": [
                                        {
                                            "color": "#444444"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "landscape",
                                    "elementType": "all",
                                    "stylers": [
                                        {
                                            "color": "#f2f2f2"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "poi",
                                    "elementType": "all",
                                    "stylers": [
                                        {
                                            "visibility": "off"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "road",
                                    "elementType": "all",
                                    "stylers": [
                                        {
                                            "saturation": -100
                                        },
                                        {
                                            "lightness": 45
                                        }
                                    ]
                                },
                                {
                                    "featureType": "road.highway",
                                    "elementType": "all",
                                    "stylers": [
                                        {
                                            "visibility": "simplified"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "road.arterial",
                                    "elementType": "labels.icon",
                                    "stylers": [
                                        {
                                            "visibility": "off"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "transit",
                                    "elementType": "all",
                                    "stylers": [
                                        {
                                            "visibility": "off"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "water",
                                    "elementType": "all",
                                    "stylers": [
                                        {
                                            "color": "#7f75b5"
                                        },
                                        {
                                            "visibility": "on"
                                        }
                                    ]
                                }';
                            }else{
                                echo '{
                                    "featureType": "all",
                                    "elementType": "labels",
                                    "stylers": [
                                        {
                                            "visibility": "on"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "all",
                                    "elementType": "labels.text.fill",
                                    "stylers": [
                                        {
                                            "saturation": 36
                                        },
                                        {
                                            "color": "#000000"
                                        },
                                        {
                                            "lightness": 40
                                        }
                                    ]
                                },
                                {
                                    "featureType": "all",
                                    "elementType": "labels.text.stroke",
                                    "stylers": [
                                        {
                                            "visibility": "on"
                                        },
                                        {
                                            "color": "#000000"
                                        },
                                        {
                                            "lightness": 16
                                        }
                                    ]
                                },
                                {
                                    "featureType": "all",
                                    "elementType": "labels.icon",
                                    "stylers": [
                                        {
                                            "visibility": "off"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "administrative",
                                    "elementType": "geometry.fill",
                                    "stylers": [
                                        {
                                            "color": "#000000"
                                        },
                                        {
                                            "lightness": 20
                                        }
                                    ]
                                },
                                {
                                    "featureType": "administrative",
                                    "elementType": "geometry.stroke",
                                    "stylers": [
                                        {
                                            "color": "#000000"
                                        },
                                        {
                                            "lightness": 17
                                        },
                                        {
                                            "weight": 1.2
                                        }
                                    ]
                                },
                                {
                                    "featureType": "administrative.country",
                                    "elementType": "labels.text.fill",
                                    "stylers": [
                                        {
                                            "color": "#be9342"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "administrative.locality",
                                    "elementType": "labels.text.fill",
                                    "stylers": [
                                        {
                                            "color": "#c4c4c4"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "administrative.neighborhood",
                                    "elementType": "labels.text.fill",
                                    "stylers": [
                                        {
                                            "color": "#be9342"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "landscape",
                                    "elementType": "geometry",
                                    "stylers": [
                                        {
                                            "color": "#000000"
                                        },
                                        {
                                            "lightness": 20
                                        }
                                    ]
                                },
                                {
                                    "featureType": "poi",
                                    "elementType": "geometry",
                                    "stylers": [
                                        {
                                            "color": "#000000"
                                        },
                                        {
                                            "lightness": 21
                                        },
                                        {
                                            "visibility": "on"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "poi.business",
                                    "elementType": "geometry",
                                    "stylers": [
                                        {
                                            "visibility": "on"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "road.highway",
                                    "elementType": "geometry.fill",
                                    "stylers": [
                                        {
                                            "color": "#be9342"
                                        },
                                        {
                                            "lightness": "0"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "road.highway",
                                    "elementType": "geometry.stroke",
                                    "stylers": [
                                        {
                                            "visibility": "off"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "road.highway",
                                    "elementType": "labels.text.fill",
                                    "stylers": [
                                        {
                                            "color": "#ffffff"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "road.highway",
                                    "elementType": "labels.text.stroke",
                                    "stylers": [
                                        {
                                            "color": "#be9342"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "road.arterial",
                                    "elementType": "geometry",
                                    "stylers": [
                                        {
                                            "color": "#000000"
                                        },
                                        {
                                            "lightness": 18
                                        }
                                    ]
                                },
                                {
                                    "featureType": "road.arterial",
                                    "elementType": "geometry.fill",
                                    "stylers": [
                                        {
                                            "color": "#575757"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "road.arterial",
                                    "elementType": "labels.text.fill",
                                    "stylers": [
                                        {
                                            "color": "#ffffff"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "road.arterial",
                                    "elementType": "labels.text.stroke",
                                    "stylers": [
                                        {
                                            "color": "#2c2c2c"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "road.local",
                                    "elementType": "geometry",
                                    "stylers": [
                                        {
                                            "color": "#000000"
                                        },
                                        {
                                            "lightness": 16
                                        }
                                    ]
                                },
                                {
                                    "featureType": "road.local",
                                    "elementType": "labels.text.fill",
                                    "stylers": [
                                        {
                                            "color": "#999999"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "transit",
                                    "elementType": "geometry",
                                    "stylers": [
                                        {
                                            "color": "#000000"
                                        },
                                        {
                                            "lightness": 19
                                        }
                                    ]
                                },
                                {
                                    "featureType": "water",
                                    "elementType": "geometry",
                                    "stylers": [
                                        {
                                            "color": "#000000"
                                        },
                                        {
                                            "lightness": 17
                                        }
                                    ]
                                }';
                            } ?>],
                        };

                        // Get the HTML DOM element that will contain your map 
                        // We are using a div with id="map" seen below in the <body>
                        var mapElement = document.getElementById('map<?php echo $i; ?>');

                        // Create the Google Map using out element and options defined above
                        var map = new google.maps.Map(mapElement, mapOptions);
						
                        var marker = new google.maps.Marker({
                            position: myLatlng,
                            map: map,
							icon: '<?php echo esc_js( $icon1 );?>',
                            title: '<?php echo esc_js($address); ?>'
                        });
						
						

                    }

    })(jQuery);   	
   	</script>
<?php

    return ob_get_clean();

}

// Testimonial Grid
add_shortcode('testig','testig_func');
function testig_func($atts, $content = null){
	extract(shortcode_atts(array(
		'number'		=>	'-1',
        'col'           =>  '2'
	), $atts));
		
	ob_start(); 
?>
	
	<div class="masonry">

        <?php 
            $args = array(   
                'post_type' => 'testimonial',   
                'posts_per_page' => $number,               
            );  
            $wp_query = new WP_Query($args);
            while ($wp_query -> have_posts()) : $wp_query -> the_post();             
            $image = wp_get_attachment_url(get_post_thumbnail_id());
            $job = get_post_meta(get_the_ID(),'_cmb_job_testi', true);
        ?>
        
        <div class="<?php if($col=='2'){echo 'col-md-6';}elseif($col=='4'){echo 'col-md-3';}else{echo 'col-md-4';} ?> item">
            <div class="de_testi">
                <blockquote>
                    <?php the_content(); ?>
                </blockquote>
                <div class="de_testi_by">
                    <span class="de_testi_pic">
                        <img src="<?php echo esc_url($image); ?>" alt="" class="img-circle"></span>
                    <div class="de_testi_company">
                        <strong><?php the_title(); ?></strong><?php if($job){ ?>, <?php echo htmlspecialchars_decode($job); ?> <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <?php endwhile; wp_reset_postdata(); ?>
    </div>

<?php
    return ob_get_clean();
}

// OT Video / Map Popup
add_shortcode('videopopup','videopopup_func');
function videopopup_func($atts, $content = null){
	extract(shortcode_atts(array(
		'photo'		=>	'',
		'link'		=>	'',
	), $atts));
		$img = wp_get_attachment_image_src($photo,'full');
		$img = $img[0];
	ob_start(); 
?>
	
	<div class="text-center">
		<a class="popup-youtube" href="<?php echo esc_url($link); ?>">
		<img src="<?php echo esc_url($img); ?>" class="img-responsive" alt="">
		</a>
		<br><br>
		<?php echo htmlspecialchars_decode($content); ?>
	</div>

<?php
    return ob_get_clean();
}

// OT Image Popup
add_shortcode('imagepopup','imagepopup_func');
function imagepopup_func($atts, $content = null){
	extract(shortcode_atts(array(
		'photo'		=>	'',
		'caption'	=>	'',
	), $atts));
		$img = wp_get_attachment_image_src($photo,'full');
		$img = $img[0];
	ob_start(); 
?>
	
	<div class="text-center">
		<a class="image-popup-no-margins" href="<?php echo esc_url($img); ?>" title="<?php echo htmlspecialchars_decode($caption); ?>">
		<img src="<?php echo esc_url($img); ?>" class="img-responsive" alt="">
		</a>
		<br><br>
	<?php echo htmlspecialchars_decode($content); ?>
	</div>

<?php
    return ob_get_clean();
}



// Menu Service
add_shortcode('menuser', 'menuser_func');
function menuser_func($atts, $content = null){
	extract(shortcode_atts(array(
		'title'		=> 	'',
		'price'		=> 	'',
		'number'   	=> 	'',
		'sale'		=>	'',
	), $atts));
				
	ob_start(); ?>

	<div class="sub-item-service">
        <div class="c1"><?php if($sale && $number){ ?><span class="disc"><?php echo esc_attr($number) ?> <?php esc_html_e('Off', 'modis'); ?></span><?php } ?><?php echo htmlspecialchars_decode($title); ?></div>
        <div class="c2"></div>
        <div class="c3"><?php echo esc_attr($price); ?></div>
    </div>

<?php
    return ob_get_clean();
}

// Call to Action
add_shortcode('call', 'call_func');
function call_func($atts, $content = null){
	extract(shortcode_atts(array(
		'icon'		=> 	'',
		'title'		=> 	'',
		'linkbox'   => 	'',
	), $atts));
		$url 	= vc_build_link( $linkbox );
	ob_start(); ?>

	
	<div class="col-md-9 mt10 text-light call-to-action">
        <h3><i class="<?php echo esc_attr($icon); ?> mr10"></i><?php echo htmlspecialchars_decode($title); ?></h3>
    </div>

    <div class="col-md-3 text-right text-center-992">
    	<?php if ( strlen( $linkbox ) > 0 && strlen( $url['url'] ) > 0 ) {
			echo '<a class="btn btn-line-white btn-big" href="' . esc_attr( $url['url'] ) . '" target="' . ( strlen( $url['target'] ) > 0 ? esc_attr( $url['target'] ) : '_self' ) . '">' . esc_attr( $url['title'] ).'</a>';
		} ?>
    </div>

<?php
    return ob_get_clean();
}

// Icon Box
add_shortcode('iconbox', 'iconbox_func');
function iconbox_func($atts, $content = null){
    extract(shortcode_atts(array(
        'icon'      =>  '',
        'title'     =>  '',
    ), $atts));
        
    ob_start(); ?>

    <div class="box-icon">
        <i class="<?php echo esc_attr($icon); ?>"></i>
        <div class="text">
            <h4><?php echo htmlspecialchars_decode($title); ?></h4>
            <p><?php echo htmlspecialchars_decode($content); ?></p>
        </div>
    </div>    

<?php
    return ob_get_clean();
}

// Infomation Box
add_shortcode('infobox', 'infobox_func');
function infobox_func($atts, $content = null){
    extract(shortcode_atts(array(
        'icon'      =>  '',
        'title'     =>  '',
    ), $atts));
        
    ob_start(); ?>

    <div class="info-box padding20">
        <i class="<?php echo esc_attr($icon); ?> id-color"></i>
        <div class="info-box_text">
            <div class="info-box_title"><?php echo htmlspecialchars_decode($title); ?></div>
            <div class="info-box_subtite"><?php echo htmlspecialchars_decode($content); ?></div>
        </div>
    </div>  

<?php
    return ob_get_clean();
}

// Image Post
add_shortcode('imagepost', 'imagepost_func');
function imagepost_func($atts, $content = null){
    extract(shortcode_atts(array(
        'photo'     =>  '',
        'title'     =>  '',
        'link'      =>  '',
    ), $atts));
        $img = wp_get_attachment_image_src($photo,'full');
        $img = $img[0];
    ob_start(); ?>

    <div class="service-item">
        <?php if($link){ ?><a href="<?php echo esc_url($link); ?>"><?php } ?>
            <img src="<?php echo esc_url($img); ?>" alt="" class="pic-grey">
            <h3><?php echo htmlspecialchars_decode($title); ?></h3>
        <?php if($link){ ?></a><?php } ?>
    </div>  

<?php
    return ob_get_clean();
}

// Image Grid
add_shortcode('imagegrid', 'imagegrid_func');
function imagegrid_func($atts, $content = null){
    extract(shortcode_atts(array(
        'photo'     =>  '',
        'col'       =>  '2',
        'grey'      =>  '',
        'padding'   =>  '',
    ), $atts));
        
    ob_start(); ?>

    <div id="gallery" class="zoom-gallery <?php if($col=='3'){echo 'gallery-3-cols';}elseif($col=='4'){echo 'gallery-4-cols';}elseif($col=='5'){echo 'gallery-5-cols';}elseif($col=='6'){echo 'gallery-6-cols';}else{echo 'gallery-2-cols';} ?>">

        <?php $img_ids = explode(",",$photo);
            foreach( $img_ids AS $img_id ){
            $meta = wp_prepare_attachment_for_js($img_id);          
            $title = $meta['title'];    
            $caption = $meta['caption'];
            $description = $meta['description'];    
            $image_src = wp_get_attachment_image_src($img_id,''); 
        ?>
            
            <div class="item <?php if($padding==true){echo 'col-md-4 margin-bottom-30';} ?>">
                <div class="picframe <?php if($grey==true){echo 'pic-grey';} ?>">
                    <a href="<?php echo esc_url($image_src[0]); ?>" title="<?php echo esc_attr($title); ?>">
                        <span class="overlay">
                            <span class="pf_text">
                                <span class="project-name"><?php echo esc_attr($title); ?></span>
                            </span>
                        </span>
                        <img src="<?php echo esc_url($image_src[0]); ?>" alt="" class="" />
                    </a>
                </div>
            </div>

        <?php } ?>

    </div>

<?php
    return ob_get_clean();
}

// Services
add_shortcode('services', 'services_func');
function services_func($atts, $content = null){
    extract(shortcode_atts(array(
        'photo'     =>  '',
        'title'     =>  '',
        'price'     =>  '',
        'btn_text'  =>  '',
        'btn_link'  =>  '',
    ), $atts));
        $img = wp_get_attachment_image_src($photo,'full');
        $img = $img[0];
    ob_start(); ?>

    <div class="picframe">
        <a href="<?php echo esc_url($btn_link); ?>">
            <span class="overlay">
                <span class="pf_text">
                    <span class="project-name"><?php echo htmlspecialchars_decode($btn_text); ?></span>
                </span>
            </span>
        </a>

        <img src="<?php echo esc_url($img); ?>" alt="" />
    </div>

    <div class="spacer-single"></div>
    <h3><?php echo htmlspecialchars_decode($title); ?> <span class="id-color pull-right"><?php echo esc_attr($price); ?></span></h3>
    <p><?php echo htmlspecialchars_decode($content); ?></p>
    <div class="spacer-half"></div>

<?php
    return ob_get_clean();
}

// Services Grid
add_shortcode('servicesg', 'servicesg_func');
function servicesg_func($atts, $content = null){
    extract(shortcode_atts(array(
        'number'    =>  '-1',
        'col'       =>  '2',
        'text'      =>  '',
        'style'     =>  'style1',
    ), $atts));

    ob_start(); ?>

    <div id="content" class="no-bottom no-top">
        <section>
            <div class="container">
                <div class="row">
                    <?php 
                        $args = array(   
                            'post_type' => 'ot_post_service',   
                            'posts_per_page' => $number,               
                        );  
                        $wp_query = new WP_Query($args);
                        while ($wp_query -> have_posts()) : $wp_query -> the_post(); 
                    ?>
                    <div class="<?php if($col=='3'){echo 'col-md-4';}elseif($col=='4'){echo 'col-md-3';}else{echo 'col-md-6';} ?> col-sm-6 text-center">
                        <div class="picframe">
                            <a href="<?php the_permalink(); ?>">
                                <span class="overlay <?php if($style!='style1'){echo 'img-circle';} ?>">
                                    <span class="pf_text">
                                        <span class="project-name"><?php echo esc_attr($text); ?></span>
                                    </span>
                                </span>
                            </a>
                            <?php if($style=='style1'){the_post_thumbnail('full', array( 'class' => '' ));}else{the_post_thumbnail('full', array( 'class' => 'img-circle' ));} ?>
                            
                        </div>

                        <div class="spacer-single"></div>
                        <h3><?php the_title(); ?></h3>
                        <p><?php the_excerpt(); ?></p>
                        <div class="spacer-half"></div>
                    </div>
                    <?php endwhile; wp_reset_postdata(); ?>  
                </div>
            </div>
        </section>
    </div>

<?php
    return ob_get_clean();
}

// Services Masonry
add_shortcode('servicesm', 'servicesm_func');
function servicesm_func($atts, $content = null){
    extract(shortcode_atts(array(
        'number'    =>  '-1',
        'col'       =>  '2',
        'desc'      =>  '',
    ), $atts));

    ob_start(); ?>

    <div id="content" class="no-bottom no-top <?php if($desc!=true){echo 'no-des';} ?>">
        <section>
            <div class="container">
                <div class="row masonry">
                    <?php 
                        $args = array(   
                            'post_type' => 'ot_post_service',   
                            'posts_per_page' => $number,               
                        );  
                        $wp_query = new WP_Query($args);
                        while ($wp_query -> have_posts()) : $wp_query -> the_post(); 
                        $des1 = get_post_meta(get_the_ID(),'_cmb_ser_content1', true);
                        $des2 = get_post_meta(get_the_ID(),'_cmb_ser_content2', true);
                    ?>
                    <div class="<?php if($col=='3'){echo 'col-md-4';}elseif($col=='4'){echo 'col-md-3';}else{echo 'col-md-6';} ?> item">
                        <div class="box-border services-mas padding30">
                            <div class="text-center">
                                <?php the_post_thumbnail('full', array( 'class' => 'img-circle img-service-thumbnail' )); ?>
                                <div class="spacer-single"></div>
                                <h1><?php the_title(); ?></h1>
                                <div class="spacer-double"></div>
                            </div>
                            <?php if($des1){ echo htmlspecialchars_decode($des1);} ?>
                            <?php if($des2){ echo htmlspecialchars_decode($des2);} ?>
                        </div>
                    </div>
                    <?php endwhile; wp_reset_postdata(); ?>
                </div>
            </div>
        </section>
    </div>

<?php
    return ob_get_clean();
}

// Services List Style
add_shortcode('servicesls', 'servicesls_func');
function servicesls_func($atts, $content = null){
    extract(shortcode_atts(array(
        'number'    =>  '-1',
        'desc'      =>  '',
    ), $atts));

    ob_start(); ?>

    <div id="content" class="no-bottom no-top <?php if($desc!=true){echo 'no-des';} ?>">
        <?php 
            $args = array(   
                'post_type' => 'ot_post_service',   
                'posts_per_page' => $number,               
            );  
            $wp_query = new WP_Query($args);
            $i=0;
            while ($wp_query -> have_posts()) : $wp_query -> the_post(); 
            $des1 = get_post_meta(get_the_ID(),'_cmb_ser_content1', true);
            $des2 = get_post_meta(get_the_ID(),'_cmb_ser_content2', true);    
            $i++;        
        ?>
        <section class="padding-top-bottom-90 services-mas <?php if($i%2==0){echo 'bg-grey';} ?>">
            <div class="container">
                <div class="row">
                    <div class="text-center">
                        <?php the_post_thumbnail('full', array( 'class' => 'img-circle img-service-thumbnail' )); ?>
                        <div class="spacer-single"></div>
                        <h1><?php the_title(); ?></h1>
                        <div class="spacer-double"></div>
                    </div>  
                    <?php if($des1){  ?>
                        <div class="col-md-6">
                            <?php echo htmlspecialchars_decode($des1); ?>
                        </div>
                    <?php } ?>
                    <?php if($des2){  ?>
                        <div class="col-md-6">
                            <?php echo htmlspecialchars_decode($des2); ?>
                        </div> 
                    <?php } ?>
                    <div class="clearfix"></div>
                </div>
            </div>
        </section>

        <?php endwhile; wp_reset_postdata();  ?>
    </div>

<?php
    return ob_get_clean();
}

// Services List Style Center Box
add_shortcode('serviceslsc', 'serviceslsc_func');
function serviceslsc_func($atts, $content = null){
    extract(shortcode_atts(array(
        'number'    =>  '-1',
        'desc'      =>  '',
    ), $atts));

    ob_start(); ?>

    <div id="content" class="no-bottom no-top <?php if($desc!=true){echo 'no-des';} ?>">
        <?php 
            $args = array(   
                'post_type' => 'ot_post_service',   
                'posts_per_page' => $number,               
            );  
            $wp_query = new WP_Query($args);
            $i = 0;
            while ($wp_query -> have_posts()) : $wp_query -> the_post(); 
            $des1 = get_post_meta(get_the_ID(),'_cmb_ser_content1', true);
            $des2 = get_post_meta(get_the_ID(),'_cmb_ser_content2', true);   
            $i++;        
        ?>
        <section class="padding-top-bottom-90 services-mas <?php if($i%2==0){echo 'bg-grey';} ?>">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2  <?php if($desc==true){echo 'list-desc';} ?>">
                        <div class="box-border padding30">
                            <div class="text-center">
                                <?php the_post_thumbnail('full', array( 'class' => 'img-circle img-service-thumbnail' )); ?>
                                <div class="spacer-single"></div>
                                <h1><?php the_title(); ?></h1>
                                <div class="spacer-double"></div>
                            </div>
                            <?php if($des1){ echo htmlspecialchars_decode($des1);} ?>
                            <?php if($des2){ echo htmlspecialchars_decode($des2);} ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php endwhile; wp_reset_postdata(); ?>
    </div>

<?php
    return ob_get_clean();
}

// Services Tab Style 1
add_shortcode('servicest1', 'servicest1_func');
function servicest1_func($atts, $content = null){
    extract(shortcode_atts(array(
        'number'      =>  '-1',
        'desc'        =>    '',
    ), $atts));

    ob_start(); ?>

    <div class="de_tab tab_style_2 scrollTo services-mas">
        <ul class="de_nav">
            <?php 
                $args = array(   
                    'post_type' => 'ot_post_service',   
                    'posts_per_page' => $number,               
                );  
                $wp_query = new WP_Query($args);
                while ($wp_query -> have_posts()) : $wp_query -> the_post();            
            ?>
            <li data-link="#section-services-tab">
                <?php the_post_thumbnail('full', array( 'class' => '' )); ?><span><?php the_title(); ?></span><div class="v-border"></div>
            </li>
            <?php endwhile; wp_reset_postdata(); ?>
        </ul>
        <div class="de_tab_content  <?php if($desc!=true){echo 'no-des';} ?>">
            <?php 
                $args = array(   
                    'post_type' => 'ot_post_service',   
                    'posts_per_page' => $number,               
                );  
                $wp_query = new WP_Query($args);
                while ($wp_query -> have_posts()) : $wp_query -> the_post(); 
                $des1 = get_post_meta(get_the_ID(),'_cmb_ser_content1', true);
                $des2 = get_post_meta(get_the_ID(),'_cmb_ser_content2', true);           
            ?>
            <div class="tab_single_content">
                <div class="row">
                    <?php if($des1){  ?>
                        <div class="col-md-6">
                            <?php echo htmlspecialchars_decode($des1); ?>
                        </div>
                    <?php } ?>
                    <?php if($des2){  ?>
                        <div class="col-md-6">
                            <?php echo htmlspecialchars_decode($des2); ?>
                        </div> 
                    <?php } ?>
                </div>
            </div>
            <?php endwhile; wp_reset_postdata(); ?>
        </div>
    </div>  

<?php
    return ob_get_clean();
}

// Services Tab Style 2
add_shortcode('servicest', 'servicest_func');
function servicest_func($atts, $content = null){
    extract(shortcode_atts(array(
        'number'      =>  '-1',
        'use'         =>  '',
        'desc'        =>  '',
    ), $atts));

    ob_start(); ?>

    <div class="de_tab tab_style_2">

        <ul class="de_nav">
            <?php 
                $args = array(   
                    'post_type' => 'ot_post_service',   
                    'posts_per_page' => $number,               
                );  
                $wp_query = new WP_Query($args);
                while ($wp_query -> have_posts()) : $wp_query -> the_post();            
            ?>
            <li data-link ="#section-services-tab"><span><?php the_title(); ?></span><div class="v-border"></div></li>
            <?php endwhile; wp_reset_postdata(); ?>
        </ul>

        <div class="de_tab_content <?php if($desc!=true){echo 'no-des';} ?>">
            <?php 
                $args = array(   
                    'post_type' => 'ot_post_service',   
                    'posts_per_page' => $number,               
                );  
                $wp_query = new WP_Query($args);
                while ($wp_query -> have_posts()) : $wp_query -> the_post(); 
                $des1 = get_post_meta(get_the_ID(),'_cmb_ser_content1', true);
                $des2 = get_post_meta(get_the_ID(),'_cmb_ser_content2', true);           
            ?>
            <div class="tab_single_content services-mas">
                <div class="row">
                    <?php if($use=='true'){ ?>
                        <div class="text-center">
                            <?php the_post_thumbnail('full', array( 'class' => 'img-circle img-service-thumbnail' )); ?>
                            <div class="spacer-double"></div>
                        </div>
                    <?php } ?>
                    <?php if($des1){  ?>
                        <div class="col-md-6">
                            <?php echo htmlspecialchars_decode($des1); ?>
                        </div>
                    <?php } ?>
                    <?php if($des2){  ?>
                        <div class="col-md-6">
                            <?php echo htmlspecialchars_decode($des2); ?>
                        </div> 
                    <?php } ?>
                </div>
            </div>
            <?php endwhile; wp_reset_postdata(); ?>
        </div>

    </div>

<?php
    return ob_get_clean();
}

// Our Facts
add_shortcode('facts', 'facts_func');
function facts_func($atts, $content = null){
    extract(shortcode_atts(array(
        'title'       =>  '',
        'icon'        =>  '',
        'number'      =>  '',
        'style'       =>  'style1',
        'width'       =>  '5',
        'align'       =>  'left'
    ), $atts));

    ob_start(); ?>

    <?php if($style=='style2'){ ?>
        <div class="text-center-992 <?php if($width=='5'){echo 'col-md-5';}elseif($width=='6'){echo 'col-md-6';}else{echo 'col-md-7';} ?> <?php if($align=='right'){echo 'pull-right text-left';}else{echo 'text-right';} ?>">
            <div class="small-padding text-light">
                <h3><?php echo htmlspecialchars_decode($title); ?></h3>
                <i class="<?php echo esc_attr($icon); ?> icon-bg"></i>
            </div>
        </div>
        <div class="<?php if($width=='5'){echo 'col-md-7';}elseif($width=='6'){echo 'col-md-6';}else{echo 'col-md-5';} ?> bg-grey bg-custom <?php if($align=='right'){echo 'text-right';} ?>">
            <div class="small-padding">
                <?php echo htmlspecialchars_decode($content); ?>
            </div>

        </div>
    <?php }else{ ?>
        <div class="de_count">
            <?php if($icon){ ?><i class="<?php echo esc_attr($icon); ?>" ></i><?php } ?>
            <h3 class="timer" data-to="<?php echo esc_attr($number); ?>" data-speed="2500"><?php echo esc_attr($number); ?></h3>
            <span><?php echo htmlspecialchars_decode($title); ?></span>
        </div>
    <?php } ?>

<?php
    return ob_get_clean();
}

// Testimonial Slider
add_shortcode('testis', 'testis_func');
function testis_func($atts, $content = null){
    extract(shortcode_atts(array(
        'number'      =>  '-1',
        'style'       =>   'style1',
    ), $atts));

    ob_start(); ?>

    <ul class="testimonial-list wow fadeIn <?php if($style=='style2'){echo 'big-font';} ?>" data-wow-delay=".25s">
        <?php 
            $args = array(   
                'post_type' => 'testimonial',   
                'posts_per_page' => $number,               
            );  
            $wp_query = new WP_Query($args);
            while ($wp_query -> have_posts()) : $wp_query -> the_post();  
            $job = get_post_meta(get_the_ID(),'_cmb_job_testi', true);
        ?>

        <li><?php the_content(); ?>
                <span><?php the_title(); ?><?php if($job){ ?>, <?php echo esc_attr($job); ?> <?php } ?></span>
        </li>

        <?php endwhile; wp_reset_postdata(); ?>
    </ul>

<?php
    return ob_get_clean();
}

// Text Auto Write
add_shortcode('write', 'write_func');
function write_func($atts, $content = null){
    extract(shortcode_atts(array(
        'size'       =>  'normal',
        'title'      =>  '',
        'auto1'      =>  '',
        'auto2'      =>  '',
        'auto3'      =>  '',
        'auto4'      =>  '',
        'auto5'      =>  '',
        'auto6'      =>  '',
    ), $atts));

    ob_start(); ?>

    <div class="type-wrap <?php if($size=='normal'){echo 'font48';}elseif($size=='small'){echo 'font30';}else{echo 'big-font';} ?> text-center">
        <?php echo htmlspecialchars_decode($title); ?>
        <div class="typed-strings">
            <?php if($auto1){ ?><p><?php echo htmlspecialchars_decode($auto1); ?></p><?php } ?>
            <?php if($auto2){ ?><p><?php echo htmlspecialchars_decode($auto2); ?></p><?php } ?>
            <?php if($auto3){ ?><p><?php echo htmlspecialchars_decode($auto3); ?></p><?php } ?>
            <?php if($auto4){ ?><p><?php echo htmlspecialchars_decode($auto4); ?></p><?php } ?>
            <?php if($auto5){ ?><p><?php echo htmlspecialchars_decode($auto5); ?></p><?php } ?>
            <?php if($auto6){ ?><p><?php echo htmlspecialchars_decode($auto6); ?></p><?php } ?>
        </div>
        <span class="typed"></span>
    </div>

    <script type="text/javascript">
        (function($) { "use strict";
            $(document).ready(function() {              
                $(".typed").typed({
                    stringsElement: $('.typed-strings'),
                    typeSpeed: 200,
                    backDelay: 1500,
                    loop: true,
                    contentType: 'html', // or text
                    // defaults to false for infinite loop
                    loopCount: false,
                    callback: function () { null; },
                    resetCallback: function () { newTyped(); }
                });
            }); 
        })(jQuery);
    </script>
<?php
    return ob_get_clean();
}

// Lastest New
add_shortcode('lastestnew', 'lastestnew_func');
function lastestnew_func($atts, $content = null){
    extract(shortcode_atts(array(
        'number'        =>  '-1',
        'excerpt'       =>  '19',
    ), $atts));

    ob_start(); ?>

    <ul id="blog-carousel" class="blog-list blog-snippet">
        <?php 
            $args = array(   
                'post_type' => 'post',   
                'posts_per_page' => $number,               
            );  
            $wp_query = new WP_Query($args);
            while ($wp_query -> have_posts()) : $wp_query -> the_post();              
        ?>
        <li class="col-md-6 item">
            <div class="post-content">
                <div class="post-image">
                    <?php the_post_thumbnail('full', array( 'class' => '' )); ?>
                </div>


                <div class="date-box">
                    <div class="day"><?php the_time('d'); ?></div>
                    <div class="month"><?php the_time('M'); ?></div>
                </div>

                <div class="post-text">
                    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                    <p><?php echo modis_blog_excerpt($excerpt); ?></p>
                </div>

            </div>
            <?php endwhile; wp_reset_postdata(); ?>
        </li>
    </ul>
    
<?php
    return ob_get_clean();
}

// Home Video Popup
add_shortcode('homevideo', 'homevideo_func');
function homevideo_func($atts, $content = null){
    extract(shortcode_atts(array(
        'link'        =>  '',
    ), $atts));

    ob_start(); ?>

    <div class="center-y text-center absolute width100">
        <div class="spacer-double"></div>

        <a href="<?php echo esc_url($link); ?>" class="popup-youtube play-button large"></a>
        <div class="spacer-single"></div>

    </div>
    
<?php
    return ob_get_clean();
}

// Home Video Background
add_shortcode('homevideob', 'homevideob_func');
function homevideob_func($atts, $content = null){
    extract(shortcode_atts(array(
        'type'      =>  'type1',
        'mp4'       =>  '',
        'webm'      =>  '',
        'link'      =>  '',
        'title'     =>  '',
        'subtitle'  =>  '',
        'linkbox'   =>  '',
        'photo'     =>  '',
        'style'     =>  'style1',
    ), $atts));
        $url    = vc_build_link( $linkbox );
        $img = wp_get_attachment_image_src($photo,'full');
        $img = $img[0];
    ob_start(); ?>

    <div class="de-video-container full-height">
        <div class="de-video-content">
            <div class="text-center">
                <?php if($style=='style1'){ ?>
                    <div class="spacer-single"></div>
                    <div class="teaser-text mb20">
                        <?php echo htmlspecialchars_decode($subtitle); ?>
                    </div>
                    <h1 class="big-font"><?php echo htmlspecialchars_decode($title); ?></h1>
                <?php }else{ ?>
                    <div class="spacer-double"></div>
                    <h1 class="big-font"><?php echo htmlspecialchars_decode($title); ?></h1>
                    <div class="teaser-text">
                        <?php echo htmlspecialchars_decode($subtitle); ?>
                    </div>
                <?php } ?>
                <div class="spacer-single"></div>
                <?php if ( strlen( $linkbox ) > 0 && strlen( $url['url'] ) > 0 ) {
                    echo '<a class="btn-slider" href="' . esc_attr( $url['url'] ) . '" target="' . ( strlen( $url['target'] ) > 0 ? esc_attr( $url['target'] ) : '_self' ) . '">' . esc_attr( $url['title'] ).'</a>';
                } ?>
            </div>
        </div>

        <div class="de-video-overlay"></div>

        <!-- load your video here -->
        <video autoplay="" loop="" muted="" poster="<?php echo esc_url($img); ?>">
            <source src="<?php echo esc_url($mp4); ?>" type="video/mp4" />
            <source src="<?php echo esc_url($webm); ?>" type="video/webm" />
        </video>

    </div>
    
<?php
    return ob_get_clean();
}

// Home Image Scale
add_shortcode('homeimgscale', 'homeimgscale_func');
function homeimgscale_func($atts, $content = null){
    extract(shortcode_atts(array(
        'title'     =>  '',
        'linkbox'   =>  '',
        'photo'     =>  '',
    ), $atts));
        $url    = vc_build_link( $linkbox );
        $img = wp_get_attachment_image_src($photo,'full');
        $img = $img[0];
    ob_start(); ?>

    <div class="autoheight padding-top-bottom-90">
        <div class="center-y text-center absolute width100 z-index-1">

            <div class="spacer-single"></div>
            <div class="spacer-half"></div>
            <h1 class="big-font"><?php echo htmlspecialchars_decode($title); ?></h1>
            <div class="spacer-single"></div>
            <?php if ( strlen( $linkbox ) > 0 && strlen( $url['url'] ) > 0 ) {
                echo '<a class="btn-slider" href="' . esc_attr( $url['url'] ) . '" target="' . ( strlen( $url['target'] ) > 0 ? esc_attr( $url['target'] ) : '_self' ) . '">' . esc_attr( $url['title'] ).'</a>';
            } ?>

        </div>
        <div class="bg-scale" data-stellar-background-ratio=".2" style="background:url('<?php echo esc_url($img); ?>');background-size: 100%;"></div>
    </div>

<?php
    return ob_get_clean();
}

// Process
add_shortcode('process', 'process_func');
function process_func($atts, $content = null){
    extract(shortcode_atts(array(
        'number'    =>  '',
        'title'     =>  '',
    ), $atts));
        
    ob_start(); ?>

    <div class="box-number square">
        <span class="number wow rotateIn animated" data-wow-delay=".5s"><?php echo esc_attr($number); ?></span>
        <div class="text">
            <h3><?php echo htmlspecialchars_decode($title); ?></h3>
            <p><?php echo htmlspecialchars_decode($content); ?></p>
        </div>
    </div>
    
<?php
    return ob_get_clean();
}



?>