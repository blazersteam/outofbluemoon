<?php $link_video = get_post_meta(get_the_ID(),'_cmb_link_video', true); ?>
<li>
    <div class="post-content">
        <div class="post-image">
            <?php if ( has_post_thumbnail() ) { ?>
                <?php if ( has_post_thumbnail() ) { ?>
                    <a href="<?php the_permalink(); ?>">
                        <?php the_post_thumbnail( 'full', array( 'class' => 'img-responsive' ) ); ?>    
                    </a>        
                <?php } ?>
            <?php }else{ ?>
                <div class="post-thumbnail">
                    <?php echo wp_oembed_get( $link_video ); ?>
                </div>
            <?php } ?>
        </div>

        <div class="date-box">
            <div class="day"><?php the_time('d'); ?></div>
            <div class="month"><?php the_time('M'); ?></div>
        </div>

        <div class="post-text">
            <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
            <p><?php echo modis_excerpt_length(); ?></p>
            <a href="<?php the_permalink(); ?>" class="btn-line"><?php esc_html_e('Read More', 'modis'); ?></a>
        </div>

    </div>
</li>