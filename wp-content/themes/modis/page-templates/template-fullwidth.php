<?php
/**
 * Template Name: Template FullWidth
 */
$sub = get_post_meta(get_the_ID(),'_cmb_page_sub', true);
get_header(); ?>

    <section id="subheader" class="subh-center" data-stellar-background-ratio=".2"
        <?php if( function_exists( 'rwmb_meta' ) ) { ?>       
            <?php $images = rwmb_meta( '_cmb_subheader_image', "type=image_advanced&size=full" ); ?>
            <?php if($images){ foreach ( $images as $image ) { ?>
            <?php $img =  $image['full_url']; ?>
              style="background-image: url('<?php echo esc_url($img); ?>');"
            <?php } } ?>
        <?php } ?> 
    >

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1><?php the_title(); ?></h1>
                    <?php if($sub != ''){ ?><h4><?php echo esc_attr($sub); ?></h4><?php } ?>
                </div>
            </div>
        </div>

    </section>
    
    <?php if (have_posts()){ ?>
            <?php while (have_posts()) : the_post()?>
                <?php the_content(); ?>
            <?php endwhile; ?>
        <?php }else {
            esc_html_e('Page Canvas For Page Builder', 'modis'); 
    }?>

<?php get_footer(); ?>