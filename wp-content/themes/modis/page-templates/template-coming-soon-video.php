<?php

/*
 * Template Name: Coming Soon Video Background
 * Description: A Page Template.
 */

//get_header(); ?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8 no-js lt-ie9" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<?php global $modis_option; ?>
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"> 
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
  <!-- Favicons
    ================================================== -->
    <?php if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) { ?>
        <!-- Favicons
        ================================================== -->
        <?php if(!empty($modis_option['favicon']['url'])){ ?>
            <link rel="shortcut icon" href="<?php echo esc_url($modis_option['favicon']['url']); ?>" type="image/png">
        <?php } ?>
        <?php if(!empty($modis_option['apple_icon']['url'])){ ?>
            <link rel="apple-touch-icon" href="<?php echo esc_url($modis_option['apple_icon']['url']); ?>">
        <?php } ?>
        <?php if(!empty($modis_option['apple_icon_72']['url'])){ ?>
            <link rel="apple-touch-icon" sizes="72x72" href="<?php echo esc_url($modis_option['apple_icon_72']['url']); ?>">
        <?php } ?>
        <?php if(!empty($modis_option['apple_icon_114']['url'])){ ?>
            <link rel="apple-touch-icon" sizes="114x114" href="<?php echo esc_url($modis_option['apple_icon_114']['url']); ?>">
        <?php } ?>
    <?php } ?>
<?php wp_head(); ?>
</head>

<body id="homepage">

    <div id="wrapper">

        <!-- content begin -->
        <div id="content" class="no-bottom no-top">
            
            <section id="hide-content" class="padding-top-bottom-90">
                <?php if (have_posts()){ ?>
                        <?php while (have_posts()) : the_post()?>
                            <?php the_content(); ?>
                        <?php endwhile; ?>
                    <?php }else {
                        esc_html_e('Page Canvas For Page Builder', 'modis'); 
                }?>
            </section>
            
            <!-- parallax section -->
            <section id="section-coming-soon" class="coming-soon padding-top-bottom-90">
                <div class="coming-soon-content text-center">
                    <img src="<?php echo esc_url($modis_option['cms_logo']['url']); ?>" alt="logo" class="logo">
                    <div class="spacer-double"></div>
                    <div class="coming-soon-text">
                        <h1><?php echo wp_kses( $modis_option['cms_title'], wp_kses_allowed_html('post') ); ?> </h1>
                        <div class="small-border"></div>
                        <span><?php echo wp_kses( $modis_option['cms_subtitle'], wp_kses_allowed_html('post') ); ?></span>
                    </div>
                    <div class="spacer-double"></div>
                    
                    <div class="col-md-4 col-md-offset-4">
                        <ul class="countdown">
                          <li> 
                            <span class="days">00</span>
                            <p class="days_ref"><?php _e('days', 'modis');  ?></p>
                          </li>
                          
                          <li>
                            <span class="hours">00</span>
                            <p class="hours_ref"><?php _e('hours', 'modis');  ?></p>
                          </li>
                          
                          <li> 
                            <span class="minutes">00</span>
                            <p class="minutes_ref"><?php _e('minutes', 'modis');  ?></p>
                          </li>
                          
                          <li>
                            <span class="seconds">00</span>
                            <p class="seconds_ref"><?php _e('seconds', 'modis');  ?></p>
                          </li>
                        </ul>
                    </div>
                    <div class="spacer-single"></div>
                    <div class="social-icons">
                        <?php if($modis_option['facebook']!=''){ ?>                                    
                                <a target="_blank" href="<?php echo esc_url($modis_option['facebook']); ?>"><i class="fa fa-facebook"></i></a>                            
                        <?php } ?>
                        <?php if($modis_option['google']!=''){ ?>                            
                                <a target="_blank" href="<?php echo esc_url($modis_option['google']); ?>"><i class="fa fa-google-plus"></i></a>                            
                        <?php } ?>
                        <?php if($modis_option['twitter']!=''){ ?>                            
                                <a target="_blank" href="<?php echo esc_url($modis_option['twitter']); ?>"><i class="fa fa-twitter"></i></a>                            
                        <?php } ?>
                        <?php if($modis_option['youtube']!=''){ ?>                            
                                <a target="_blank" href="<?php echo esc_url($modis_option['youtube']); ?>"><i class="fa fa-youtube"></i></a>                            
                        <?php } ?>
                        <?php if($modis_option['linkedin']!=''){ ?>
                                <a target="_blank" href="<?php echo esc_url($modis_option['linkedin']); ?>"><i class="fa fa-linkedin"></i></a>
                        <?php } ?>
                        <?php if($modis_option['dribbble']!=''){ ?>
                                <a target="_blank" href="<?php echo esc_url($modis_option['dribbble']); ?>"><i class="fa fa-dribbble"></i></a>
                        <?php } ?>
                        <?php if($modis_option['pinterest']!=''){ ?>
                                <a target="_blank" href="<?php echo esc_url($modis_option['pinterest']); ?>"><i class="fa fa-pinterest"></i></a>
                        <?php } ?>
                        <?php if($modis_option['instagram']!=''){ ?>
                                <a target="_blank" href="<?php echo esc_url($modis_option['instagram']); ?>"><i class="fa fa-instagram"></i></a>
                        <?php } ?>  
                        <?php if($modis_option['github']!=''){ ?>
                                <a target="_blank" href="<?php echo esc_url($modis_option['github']); ?>"><i class="fa fa-github"></i></a>
                        <?php } ?>
                        <?php if($modis_option['vimeo']!=''){ ?>
                            <a target="_blank" href="<?php echo esc_url($modis_option['vimeo']); ?>"><i class="fa fa-vimeo-square"></i></a>
                        <?php } ?>
                        <?php if($modis_option['tumblr']!=''){ ?>
                            <a target="_blank" href="<?php echo esc_url($modis_option['tumblr']); ?>"><i class="fa fa-tumblr-square"></i></a>
                        <?php } ?>
                        <?php if($modis_option['soundcloud']!=''){ ?>
                            <a target="_blank" href="<?php echo esc_url($modis_option['soundcloud']); ?>"><i class="fa fa-soundcloud"></i></a>
                        <?php } ?>
                        <?php if($modis_option['behance']!=''){ ?>
                            <a target="_blank" href="<?php echo esc_url($modis_option['behance']); ?>"><i class="fa  fa-behance"></i></a>
                        <?php } ?>
                        <?php if($modis_option['lastfm']!=''){ ?>
                            <a target="_blank" href="<?php echo esc_url($modis_option['lastfm']); ?>"><i class="fa fa-lastfm"></i></a>
                        <?php } ?> 
                        <?php if($modis_option['skype']!=''){ ?>
                            <a target="_blank" href="<?php echo esc_url($modis_option['skype']); ?>"><i class="fa fa-skype"></i></a>
                        <?php } ?> 
                        <?php if($modis_option['rss']!=''){ ?>
                            <a target="_blank" href="<?php echo esc_url($modis_option['rss']); ?>"><i class="fa fa-rss"></i></a>
                        <?php } ?>
                    </div>
                </div>
            </section>
            <!-- parallax section close -->

            <section id="section-bg-video" aria-label="section-bg-video" class="section-fixed no-top no-bottom">
                <div class="de-video-overlay"></div>

                    <!-- load your video here -->
                    <video autoplay="" loop="" muted="" poster="<?php echo esc_url($modis_option['cms_bg_video']['url']); ?>">
                        <source src="<?php echo esc_url($modis_option['mp4']); ?>" type="video/mp4" />
                        <source src="<?php echo esc_url($modis_option['webm']); ?>" type="video/webm" />
                    </video>
            </section>
            
            <?php if($modis_option['cms_hide']!=false){ ?>
              <div class="arrow-down"></div>
              <div class="arrow-up"></div>
            <?php } ?>

        </div>
    </div>    
    <script type="text/javascript">
  /**
   * downCount: Simple Countdown clock with offset
   * Author: Sonny T. <hi@sonnyt.com>, sonnyt.com
   */

  (function ($) {
    $.fn.downCount = function (options, callback) {
      var settings = $.extend({
          date: null,
          offset: null
        }, options);

      // Throw error if date is not set
      if (!settings.date) {
        $.error('Date is not defined.');
      }

      // Throw error if date is set incorectly
      if (!Date.parse(settings.date)) {
        $.error('Incorrect date format, it should look like this, 12/24/2012 12:00:00.');
      }

      // Save container
      var container = this;

      /**
       * Change client's local date to match offset timezone
       * @return {Object} Fixed Date object.
       */
      var currentDate = function () {
        // get client's current date
        var date = new Date();

        // turn date to utc
        var utc = date.getTime() + (date.getTimezoneOffset() * 60000);

        // set new Date object
        var new_date = new Date(utc + (3600000*settings.offset))

        return new_date;
      };

      /**
       * Main downCount function that calculates everything
       */
      function countdown () {
        var target_date = new Date(settings.date), // set target date
          current_date = currentDate(); // get fixed current date

        // difference of dates
        var difference = target_date - current_date;

        // if difference is negative than it's pass the target date
        if (difference < 0) {
          // stop timer
          clearInterval(interval);

          if (callback && typeof callback === 'function') callback();

          return;
        }

        // basic math variables
        var _second = 1000,
          _minute = _second * 60,
          _hour = _minute * 60,
          _day = _hour * 24;

        // calculate dates
        var days = Math.floor(difference / _day),
          hours = Math.floor((difference % _day) / _hour),
          minutes = Math.floor((difference % _hour) / _minute),
          seconds = Math.floor((difference % _minute) / _second);

          // fix dates so that it will show two digets
          days = (String(days).length >= 2) ? days : '0' + days;
          hours = (String(hours).length >= 2) ? hours : '0' + hours;
          minutes = (String(minutes).length >= 2) ? minutes : '0' + minutes;
          seconds = (String(seconds).length >= 2) ? seconds : '0' + seconds;

        // based on the date change the refrence wording
        var ref_days = (days === 1) ? 'day' : '<?php _e('days', 'modis');  ?>',
          ref_hours = (hours === 1) ? 'hour' : '<?php _e('hours', 'modis');  ?>',
          ref_minutes = (minutes === 1) ? 'minute' : '<?php _e('minutes', 'modis');  ?>',
          ref_seconds = (seconds === 1) ? 'second' : '<?php _e('seconds', 'modis');  ?>';
          

        // set to DOM
        container.find('.days').text(days);
        container.find('.hours').text(hours);
        container.find('.minutes').text(minutes);
        container.find('.seconds').text(seconds);

        container.find('.days_ref').text(ref_days);
        container.find('.hours_ref').text(ref_hours);
        container.find('.minutes_ref').text(ref_minutes);
        container.find('.seconds_ref').text(ref_seconds);
      };
      
      // start
      var interval = setInterval(countdown, 1000);
    };

  })(jQuery); 
  
  (function($) { "use strict";      
      //Timer
      $('.countdown').downCount({
        date: '<?php echo htmlspecialchars_decode($modis_option['cms_date']); ?> 12:00:00',
        offset: +10
      }, function () {
        alert('WOOT WOOT, done!');
      });
      //Portfolio Top Sections Fullscreen         
      $(function(){"use strict";
      $('.commingsoon-top').css({'height':($(window).height())+'px'});
      $(window).resize(function(){
      $('.commingsoon-top').css({'height':($(window).height())+'px'});
      });
      });
      
  })(jQuery);
</script>

</body>
<?php wp_footer(); ?>
</body>
</html>