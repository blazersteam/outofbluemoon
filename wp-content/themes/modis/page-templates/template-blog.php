<?php
/**
 * Template Name: Template Blog
 */
get_header(); 
$sub = get_post_meta(get_the_ID(),'_cmb_page_sub', true);
?>

    <section id="subheader" class="subh-center" data-stellar-background-ratio=".2"
        <?php if( function_exists( 'rwmb_meta' ) ) { ?>       
            <?php $images = rwmb_meta( '_cmb_subheader_image', "type=image_advanced&size=full" ); ?>
            <?php if($images){ foreach ( $images as $image ) { ?>
            <?php $img =  $image['full_url']; ?>
              style="background-image: url('<?php echo esc_url($img); ?>');"
            <?php } } ?>
        <?php } ?>
    >

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1><?php the_title(); ?></h1>
                    <h4><?php echo esc_attr($sub); ?></h4>
                </div>
            </div>
        </div>

    </section>

    <div id="content">
        <div class="container">
            <div class="row">

                <?php if($modis_option['blog_style']=='style2'){ ?>
                    <div class="col-md-4">
                        <?php get_sidebar();?>  
                    </div>
                <?php } ?>

                <div class="<?php if($modis_option['blog_style']=='style3'){echo 'col-md-12';}else{echo 'col-md-8';} ?>">
                    <ul class="blog-list">
                        <?php 
                            $args = array(    
                                'paged' => $paged,
                                'post_type' => 'post',
                            );
                            $wp_query = new WP_Query($args);
                            while ($wp_query -> have_posts()): $wp_query -> the_post();                         
                            get_template_part( 'content', get_post_format() ) ; 
                        ?> 
                        <?php endwhile;?> 
                    </ul>
                    <div class="text-center">
                        <?php echo modis_pagination(); ?>    
                    </div>
                </div>

                <?php if($modis_option['blog_style']=='style1'){ ?>
                    <div class="col-md-4">
                        <?php get_sidebar();?>  
                    </div>
                <?php } ?>

            </div>
        </div>
    </div>
        
<?php get_footer(); ?>
