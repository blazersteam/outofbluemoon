<li>
    <div class="post-content">
        <div class="post-image">
            <?php if ( has_post_thumbnail() ) { ?>
                <a href="<?php the_permalink(); ?>">
                    <?php the_post_thumbnail( 'full', array( 'class' => 'img-responsive' ) ); ?>    
                </a>        
            <?php } ?>            
        </div>

        <div class="<?php if(has_post_thumbnail()){ echo 'date-box'; }else{echo 'date-box blog-no-image';} ?>">
            <div class="day"><?php the_time('d'); ?></div>
            <div class="month"><?php the_time('M'); ?></div>
        </div>

        <div class="post-text">
            <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
            <p><?php echo modis_excerpt_length(); ?></p>
            <a href="<?php the_permalink(); ?>" class="btn-line"><?php esc_html_e('Read More', 'modis'); ?></a>
        </div>

    </div>
</li>