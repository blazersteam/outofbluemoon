<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package modis
 */
global $wp_query;
$subtitle = get_post_meta($wp_query->get_queried_object_id(), "_cmb_page_sub", true);
get_header(); ?>
    
<!-- subheader -->
<section id="subheader" class="subh-center" data-stellar-background-ratio=".2"
    <?php if( function_exists( 'rwmb_meta' ) ) { ?>       
        <?php 
            global $wp_query;
            $images = rwmb_meta( '_cmb_subheader_image', 'type=image_advanced&size=full', $wp_query->get_queried_object_id() );
        ?>
        <?php if($images != ''){ foreach ( $images as $image ) { ?>
        <?php $img =  $image['full_url']; ?>
          style="background-image: url('<?php echo esc_url($img); ?>');"
        <?php } } ?>
    <?php } ?>
>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1><?php echo get_the_title( get_option( 'page_for_posts' ) ); ?></h1>  
                <?php if($subtitle != ''){ ?><h4><?php echo esc_attr($subtitle); ?></h4><?php } ?>
            </div>
        </div>
    </div>
</section>
<!-- subheader close -->

<div id="content">
    <div class="container">
        <div class="row">
        
            <?php if($modis_option['blog_style']=='style2'){ ?>
                <div class="col-md-4">
                    <?php get_sidebar();?>  
                </div>
            <?php } ?>

            <div class="<?php if($modis_option['blog_style']=='style3'){echo 'col-md-12';}else{echo 'col-md-8';} ?>">
                <ul class="blog-list">
                    <?php 
                        while (have_posts()) : the_post();
                            get_template_part( 'content', get_post_format() ) ;   // End the loop.
                        endwhile;
                    ?>
                </ul>
                <div class="text-center">
                    <?php echo modis_pagination(); ?>    
                </div>
            </div>

            <?php if($modis_option['blog_style']=='style1'){ ?>
                <div class="col-md-4">
                    <?php get_sidebar();?>  
                </div>
            <?php } ?>

        </div>
    </div>
</div>

<?php get_footer(); ?>
