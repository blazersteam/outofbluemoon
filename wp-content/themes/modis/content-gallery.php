<li>
    <div class="post-content">
        <div class="post-image">
            <?php if ( has_post_thumbnail() ) { ?>
                <a href="<?php the_permalink(); ?>">
                    <?php the_post_thumbnail( 'full', array( 'class' => 'img-responsive' ) ); ?>    
                </a>        
            <?php }else{ ?>
                <div  id="blog-carousel-<?php the_ID() ?>" class="blog-carousel">
                    <?php if( function_exists( 'rwmb_meta' ) ) { ?>
                        <?php $images = rwmb_meta( '_cmb_images', "type=image" ); ?>
                        <?php if($images){ ?>              
                            <?php  foreach ( $images as $image ) {  ?>
                                <?php $img = $image['full_url']; ?>
                                <div class="item"><img src="<?php echo esc_url($img); ?>" alt=""></div>
                            <?php } ?>                
                        <?php } ?>

                    <?php } ?>
                </div>
            <?php } ?>
        </div>

        <div class="date-box">
            <div class="day"><?php the_time('d'); ?></div>
            <div class="month"><?php the_time('M'); ?></div>
        </div>

        <div class="post-text">
            <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
            <p><?php echo modis_excerpt_length(); ?></p>
            <a href="<?php the_permalink(); ?>" class="btn-line"><?php esc_html_e('Read More', 'modis'); ?></a>
        </div>

    </div>
</li>
<script type="text/javascript">
    (function($) { "use strict";
        $(document).ready(function() {
            $("#blog-carousel-<?php the_ID(); ?>").owlCarousel({
                pagination : true,
                navigation:false,
                transitionStyle : "fade",
                slideSpeed : 500,
                paginationSpeed : 500,
                singleItem:true,
                autoPlay: 5000,
                navigationText: [
                    "<i class='fa fa fa-angle-left'></i>",
                    "<i class='fa fa fa-angle-right'></i>"
                ],
            });
        });
    })(jQuery);
</script>