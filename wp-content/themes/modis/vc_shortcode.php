<?php 
// OT Heading
if(function_exists('vc_map')){
   vc_map( array(
   "name" => esc_html__("OT Heading", 'modis'),
   "base" => "heading",
   "class" => "",
   "category" => 'Modis Elements',
   "icon" => "icon-st",
   "params" => array( 
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Title", 'modis'),
         "param_name" => "title",
         "value" => "",
         "description" => esc_html__("Title of heading", 'modis')
      ), 
      array(
         "type" => "dropdown",
         "heading" => esc_html__('Element Tag', 'modis'),
         "param_name" => "tag",
         "value" => array(                        
                     esc_html__('h1', 'modis')   => 'h1',                     
                     esc_html__('h2', 'modis')   => 'h2',
                     esc_html__('h3', 'modis')   => 'h3',
                     esc_html__('h4', 'modis')   => 'h4',
                     esc_html__('h5', 'modis')   => 'h5',
                     esc_html__('h6', 'modis')   => 'h6',
                     ), 
      ),
      array(
         "type" => "dropdown",
         "heading" => esc_html__('Text Align', 'modis'),
         "param_name" => "align",
         "value" => array(                        
                     esc_html__('Left', 'modis')   => 'left',                     
                     esc_html__('Right', 'modis')   => 'right',
                     esc_html__('Center', 'modis')   => 'center',
                     ), 
      ), 
      array(
         "type" => "checkbox",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__('Use Underline', 'modis'),
         "param_name" => "line",
      ),
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Extra Class", 'modis'),
         "param_name" => "el_class",
         "value" => "",
         "description" => esc_html__("Add class for CSS.", 'modis')
      ), 
   )));
}

// Button
if(function_exists('vc_map')){
   vc_map( array(
   "name" => esc_html__("OT Button", 'modis'),
   "base" => "button",
   "class" => "",
   "category" => 'Modis Elements',
   "icon" => "icon-st",
   "params" => array(      
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Button Text", 'modis'),
         "param_name" => "text",
         "value" => "",
      ), 
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Button Link", 'modis'),
         "param_name" => "link",
         "value" => "",
      ), 
      array(
         "type" => "dropdown",
         "heading" => esc_html__('Button Size', 'modis'),
         "param_name" => "size",
         "value" => array(                        
                     esc_html__('Normal', 'modis')   => 'normal',
                     esc_html__('Big', 'modis')      => 'big',
                     ), 
      ), 
      array(
         "type" => "dropdown",
         "heading" => esc_html__('Button Style', 'modis'),
         "param_name" => "style",
         "value" => array(                        
                     esc_html__('Style 1 : Color And Border White', 'modis')   => 'style1',
                     esc_html__('Style 2 : Color And Border Main Color', 'modis')      => 'style2',
                     esc_html__('Style 3 : Backgroung Main Color', 'modis')      => 'style3',
                     ), 
      ), 
   )));
}

// OT About
if(function_exists('vc_map')){
   vc_map( array(
   "name" => esc_html__("OT About", 'modis'),
   "base" => "about",
   "class" => "",
   "category" => 'Modis Elements',
   "icon" => "icon-st",
   "params" => array(      
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Title", 'modis'),
         "param_name" => "title",
         "value" => "",
         "description" => esc_html__("Title of about", 'modis')
      ),  
      array(
         "type" => "textarea_html",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Description", 'modis'),
         "param_name" => "content",
         "value" => "",
         "description" => esc_html__("content right.", 'modis')
      ),
   )));
}

// OT Team
if(function_exists('vc_map')){
   vc_map( array(
   "name" => esc_html__("OT Team", 'modis'),
   "base" => "team",
   "class" => "",
   "category" => 'Modis Elements',
   "icon" => "icon-st",
   "params" => array(
      array(
         "type" => "attach_image",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Photo", 'modis'),
         "param_name" => "photo",
         "value" => "",
         "description" => esc_html__("Photo of person.", 'modis')
      ),
      array(
         "type" => "checkbox",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__('Use Picture Grey', 'modis'),
         "param_name" => "use",
      ),       
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Name", 'modis'),
         "param_name" => "name",
         "value" => "",
         "description" => esc_html__("Name of person", 'modis')
      ),  
      array(
         "type" => "textarea_html",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Description", 'modis'),
         "param_name" => "content",
         "value" => "",
         "description" => esc_html__("content right.", 'modis')
      ),
      array(
         "type" => "iconpicker",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Contact 1", 'modis'),
         "param_name" => "con1",
         "value" => "",
      ), 
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Link Contact 1", 'modis'),
         "param_name" => "url1",
         "value" => "",
      ), 
      array(
         "type" => "iconpicker",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Contact 2", 'modis'),
         "param_name" => "con2",
         "value" => "",
      ), 
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Link Contact 2", 'modis'),
         "param_name" => "url2",
         "value" => "",
      ), 
      array(
         "type" => "iconpicker",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Contact 3", 'modis'),
         "param_name" => "con3",
         "value" => "",
      ), 
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Link Contact 3", 'modis'),
         "param_name" => "url3",
         "value" => "",
      ), 
      array(
         "type" => "iconpicker",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Contact 4", 'modis'),
         "param_name" => "con4",
         "value" => "",
      ), 
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Link Contact 4", 'modis'),
         "param_name" => "url4",
         "value" => "",
      ), 
   )));
}

// // OT Fun Facts
// if(function_exists('vc_map')){
//    vc_map( array(
//    "name" => esc_html__("OT Fun Facts", 'modis'),
//    "base" => "facts",
//    "class" => "",
//    "category" => 'Modis Elements',
//    "icon" => "icon-st",
//    "params" => array( 
//       array(
//          "type" => "textfield",
//          "holder" => "div",
//          "class" => "",
//          "heading" => esc_html__("Title", 'modis'),
//          "param_name" => "title",
//          "value" => "",
//          "description" => esc_html__("Title of heading", 'modis')
//       ), 
//       array(
//          "type" => "iconpicker",
//          "holder" => "div",
//          "class" => "",
//          "heading" => esc_html__("Icon", 'modis'),
//          "param_name" => "icon",
//          "value" => "",
//       ),
//       array(
//          "type" => "dropdown",
//          "heading" => esc_html__('Icon Align', 'modis'),
//          "param_name" => "align",
//          "value" => array(                        
//                      esc_html__('Left', 'modis')   => 'left',
//                      esc_html__('Right', 'modis')   => 'right',
//                      ), 
//       ), 
//       array(
//          "type" => "textarea_html",
//          "holder" => "div",
//          "class" => "",
//          "heading" => esc_html__("Description", 'modis'),
//          "param_name" => "content",
//          "value" => "",
//          "description" => esc_html__("content right.", 'modis')
//       ),
//    )));
// }

// OT Pricing Table
if(function_exists('vc_map')){
   vc_map( array(
   "name" => esc_html__("OT Pricing Table", 'modis'),
   "base" => "pricing",
   "class" => "",
   "category" => 'Modis Elements',
   "icon" => "icon-st",
   "params" => array( 
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Title", 'modis'),
         "param_name" => "title",
         "value" => "",
      ),       
      array(
         "type" => "textarea_html",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Description", 'modis'),
         "param_name" => "content",
         "value" => "",
         "description" => esc_html__("content right.", 'modis')
      ),
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Status", 'modis'),
         "param_name" => "status",
         "value" => "",
      ),
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Price", 'modis'),
         "param_name" => "price",
         "value" => "",
      ),
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Unit", 'modis'),
         "param_name" => "unit",
         "value" => "",
      ),
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Sale", 'modis'),
         "param_name" => "sale",
         "value" => "",
      ),
      array(
        'type' => 'vc_link',
         "heading" => esc_html__("Button", 'modis'),
         "param_name" => "linkbox",       
         "description" => esc_html__("Add link to Button.", 'modis'),
      ),
   )));
}

// OT Our Skill
if(function_exists('vc_map')){
   vc_map( array(
   "name" => esc_html__("OT Our Skill", 'modis'),
   "base" => "skill",
   "class" => "",
   "category" => 'Modis Elements',
   "icon" => "icon-st",
   "params" => array( 
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Title", 'modis'),
         "param_name" => "title",
         "value" => "",
      ), 
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Number", 'modis'),
         "param_name" => "number",
         "value" => "",
      ), 
   )));
}

//Google Map
if(function_exists('vc_map')){
   vc_map( array(
   "name" => __("OT Google Map", 'modis'),
   "base" => "ggmap",
   "class" => "",
   "icon" => "icon-st",
   "category" => 'Modis Elements',
   "params" => array(        
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => __("Height Map", 'modis'),
         "param_name" => "height",
         "value" => 320,
         "description" => __("Please enter number height Map, 300, 350, 380, ..etc. Default: 420.", 'modis')
      ),    
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => __("Latitude", 'modis'),
         "param_name" => "lat",
         "value" => -6.373091,
         "description" => __("Please enter <a href='http://www.latlong.net/'>Latitude</a> google map", 'modis')
      ),
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => __("Longitude", 'modis'),
         "param_name" => "long",
         "value" => 106.835175,
         "description" => __("Please enter <a href='http://www.latlong.net/'>Longitude</a> google map", 'modis')

      ),
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => __("Zoom Map", 'modis'),
         "param_name" => "zoom",
         "value" => 15,
         "description" => __("Please enter Zoom Map, Default: 15", 'modis')
      ),    
      array(
         "type" => "attach_image",
         "holder" => "div",
         "class" => "",
         "heading" => "Icon Map marker",
         "param_name" => "icon",
         "value" => "",
         "description" => __("Icon Map marker, 85 x 85", 'modis')
      ),
      array(
         "type" => "dropdown",
         "heading" => esc_html__('Style', 'modis'),
         "param_name" => "style",
         "value" => array(
                     esc_html__('Light', 'modis')     => 'light',
                     esc_html__('Dark', 'modis')     => 'dark', 
                     ), 
      ),
    )));
}

// Testimonial Grid
if(function_exists('vc_map')){
   vc_map( array(
   "name" => __("OT Testimonial Grid", 'modis'),
   "base" => "testig",
   "class" => "",
   "icon" => "icon-st",
   "category" => 'Modis Elements',
   "params" => array(  
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Number Show", 'modis'),
         "param_name" => "number",
         "value" => "",
      ), 
      array(
         "type" => "dropdown",
         "heading" => esc_html__('Columns', 'modis'),
         "param_name" => "col",
         "value" => array(
                     esc_html__('2', 'modis')     => '2',
                     esc_html__('3', 'modis')     => '3', 
                     esc_html__('4', 'modis')     => '4',
                     ), 
      ),
    )));
}

// Image Popup
if(function_exists('vc_map')){
   vc_map( array(
   "name" => __("OT Image Popup", 'modis'),
   "base" => "imagepopup",
   "class" => "",
   "icon" => "icon-st",
   "category" => 'Modis Elements',
   "params" => array(        
      array(
         "type" => "attach_image",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Photo", 'modis'),
         "param_name" => "photo",
         "value" => "",
      ),      
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Caption", 'modis'),
         "param_name" => "caption",
         "value" => "",
         "description" => esc_html__("Title of image", 'modis')
      ),      
      array(
         "type" => "textarea_html",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Description", 'modis'),
         "param_name" => "content",
         "value" => "",
         "description" => esc_html__("content right.", 'modis')
      ),
    )));
}

// Video/Map Popup
if(function_exists('vc_map')){
   vc_map( array(
   "name" => __("OT Video/Map Popup", 'modis'),
   "base" => "videopopup",
   "class" => "",
   "icon" => "icon-st",
   "category" => 'Modis Elements',
   "params" => array(        
      array(
         "type" => "attach_image",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Photo", 'modis'),
         "param_name" => "photo",
         "value" => "",
      ),  
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Link Video / Google Map", 'modis'),
         "param_name" => "link",
         "value" => "",
      ),         
      array(
         "type" => "textarea_html",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Description", 'modis'),
         "param_name" => "content",
         "value" => "",
         "description" => esc_html__("content right.", 'modis')
      ),
    )));
}



// Menu Service
if(function_exists('vc_map')){
   vc_map( array(
   "name" => esc_html__("OT Menu Service", 'modis'),
   "base" => "menuser",
   "class" => "",
   "icon" => "icon-st",
   "category" => 'Modis Elements',
   "params" => array( 
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => "Title",
         "param_name" => "title",
         "value" => "",
         "description" => esc_html__("Title of service", "modis")
      ),
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => "Price",
         "param_name" => "price",
         "value" => "",
      ),  
      array(
         "type" => "checkbox",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__('Sale', 'modis'),
         "param_name" => "sale",
      ), 
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Sale Number", 'modis'),
         "param_name" => "number",
         "dependency"  => array( 'element' => 'sale', 'value' => "true" ),
      ),
    )));
}

// Call To Action
if(function_exists('vc_map')){
   vc_map( array(
   "name" => esc_html__("OT Call To Action", 'modis'),
   "base" => "call",
   "class" => "",
   "icon" => "icon-st",
   "category" => 'Modis Elements',
   "params" => array( 
      array(
         "type" => "iconpicker",
         "holder" => "div",
         "class" => "",
         "heading" => "Icon",
         "param_name" => "icon",
         "value" => "",
      ),
      array(
         "type" => "textarea",
         "holder" => "div",
         "class" => "",
         "heading" => "Title",
         "param_name" => "title",
         "value" => "",
      ),      
      array(
        'type' => 'vc_link',
         "heading" => esc_html__("Button", 'modis'),
         "param_name" => "linkbox",        
         "description" => esc_html__("Add link to Button.", 'modis'),
      ),
    )));
}

// Icon Box
if(function_exists('vc_map')){
   vc_map( array(
   "name" => esc_html__("OT Icon Box", 'modis'),
   "base" => "iconbox",
   "class" => "",
   "icon" => "icon-st",
   "category" => 'Modis Elements',
   "params" => array( 
      array(
         "type" => "iconpicker",
         "holder" => "div",
         "class" => "",
         "heading" => "Icon",
         "param_name" => "icon",
         "value" => "",
      ),
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => "Title",
         "param_name" => "title",
         "value" => "",
      ),      
      array(
         "type" => "textarea_html",
         "holder" => "div",
         "class" => "",
         "heading" => "Content",
         "param_name" => "content",
         "value" => "",
      ),  
    )));
}

// Infomation Box
if(function_exists('vc_map')){
   vc_map( array(
   "name" => esc_html__("OT Infomation Box", 'modis'),
   "base" => "infobox",
   "class" => "",
   "icon" => "icon-st",
   "category" => 'Modis Elements',
   "params" => array( 
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => "Icon",
         "param_name" => "icon",
         "value" => "",
         "description" => wp_kses("Enter icon ex: icon_clock_alt . Find icon <a href='https://www.elegantthemes.com/blog/resources/elegant-icon-font'>here</a>", 'modis'),
      ),
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => "Title",
         "param_name" => "title",
         "value" => "",
      ),      
      array(
         "type" => "textarea_html",
         "holder" => "div",
         "class" => "",
         "heading" => "Content",
         "param_name" => "content",
         "value" => "",
      ),  
    )));
}

// Image Post
if(function_exists('vc_map')){
   vc_map( array(
   "name" => esc_html__("OT Image Post", 'modis'),
   "base" => "imagepost",
   "class" => "",
   "icon" => "icon-st",
   "category" => 'Modis Elements',
   "params" => array( 
      array(
         "type" => "attach_image",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Photo", 'modis'),
         "param_name" => "photo",
         "value" => "",
      ),  
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => "Title",
         "param_name" => "title",
         "value" => "",
      ),  
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => "Link",
         "param_name" => "link",
         "value" => "",
      ),  
    )));
}

// Image Grid
if(function_exists('vc_map')){
   vc_map( array(
   "name" => esc_html__("OT Image Grid", 'modis'),
   "base" => "imagegrid",
   "class" => "",
   "icon" => "icon-st",
   "category" => 'Modis Elements',
   "params" => array( 
      array(
         "type" => "attach_images",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Photo", 'modis'),
         "param_name" => "photo",
         "value" => "",
      ), 
      array(
         "type" => "checkbox",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__('Use Grey Image', 'modis'),
         "param_name" => "grey",
      ),
      array(
         "type" => "checkbox",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__('Item Padding', 'modis'),
         "param_name" => "padding",
      ),
      array(
         "type" => "dropdown",
         "heading" => esc_html__('Columns', 'modis'),
         "param_name" => "col",
         "value" => array(
                     esc_html__('2 Columns', 'modis')     => '2',
                     esc_html__('3 Columns', 'modis')     => '3', 
                     esc_html__('4 Columns', 'modis')     => '4',
                     esc_html__('5 Columns', 'modis')     => '5',
                     esc_html__('6 Columns', 'modis')     => '6',
                     ), 
      ),  
    )));
}

// Services 
if(function_exists('vc_map')){
   vc_map( array(
   "name" => esc_html__("OT Services", 'modis'),
   "base" => "services",
   "class" => "",
   "icon" => "icon-st",
   "category" => 'Modis Elements',
   "params" => array( 
      array(
         "type" => "attach_image",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Image", 'modis'),
         "param_name" => "photo",
         "value" => "",
      ),
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => "Title",
         "param_name" => "title",
         "value" => "",
      ), 
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => "Price",
         "param_name" => "price",
         "value" => "",
      ), 
      array(
         "type" => "textarea_html",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__('Content', 'modis'),
         "param_name" => "content",
      ),
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => "Button Text",
         "param_name" => "btn_text",
         "value" => "",
      ),  
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => "Button Link",
         "param_name" => "btn_link",
         "value" => "",
      ), 
   )));
}

// Service Grid
if(function_exists('vc_map')){
   vc_map( array(
   "name" => esc_html__("OT Services Grid", 'modis'),
   "base" => "servicesg",
   "class" => "",
   "icon" => "icon-st",
   "category" => 'Modis Elements',
   "params" => array( 
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => "Number Show",
         "param_name" => "number",
         "value" => "",
      ),  
      array(
         "type" => "dropdown",
         "heading" => esc_html__('Columns', 'modis'),
         "param_name" => "col",
         "value" => array(
                     esc_html__('2', 'modis')     => '2',
                     esc_html__('3', 'modis')     => '3', 
                     esc_html__('4', 'modis')     => '4',
                     ), 
      ),
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => "Text Image Hover",
         "param_name" => "text",
         "value" => "",
      ),  
      array(
         "type" => "dropdown",
         "heading" => esc_html__('Style', 'modis'),
         "param_name" => "style",
         "value" => array(
                     esc_html__('Image Square', 'modis')     => 'style1',
                     esc_html__('Image Circle', 'modis')     => 'style2', 
                     ), 
      ),
    )));
}

// Service Masonry
if(function_exists('vc_map')){
   vc_map( array(
   "name" => esc_html__("OT Services Masonry", 'modis'),
   "base" => "servicesm",
   "class" => "",
   "icon" => "icon-st",
   "category" => 'Modis Elements',
   "params" => array( 
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => "Number Show",
         "param_name" => "number",
         "value" => "",
      ),  
      array(
         "type" => "dropdown",
         "heading" => esc_html__('Columns', 'modis'),
         "param_name" => "col",
         "value" => array(
                     esc_html__('2', 'modis')     => '2',
                     esc_html__('3', 'modis')     => '3', 
                     esc_html__('4', 'modis')     => '4',
                     ), 
      ),
      array(
         "type" => "checkbox",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__('Use Description', 'modis'),
         "param_name" => "desc",
      ), 
    )));
}

// Service List Style
if(function_exists('vc_map')){
   vc_map( array(
   "name" => esc_html__("OT Services List Style", 'modis'),
   "base" => "servicesls",
   "class" => "",
   "icon" => "icon-st",
   "category" => 'Modis Elements',
   "params" => array( 
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => "Number Show",
         "param_name" => "number",
         "value" => "",
      ), 
      array(
         "type" => "checkbox",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__('Use Description', 'modis'),
         "param_name" => "desc",
      ), 
    )));
}

// Service List Style Center Box
if(function_exists('vc_map')){
   vc_map( array(
   "name" => esc_html__("OT Services List Style Center Box", 'modis'),
   "base" => "serviceslsc",
   "class" => "",
   "icon" => "icon-st",
   "category" => 'Modis Elements',
   "params" => array( 
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => "Number Show",
         "param_name" => "number",
         "value" => "",
      ), 
      array(
         "type" => "checkbox",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__('Use Description', 'modis'),
         "param_name" => "desc",
      ), 
    )));
}

// Service Tab Style 1
if(function_exists('vc_map')){
   vc_map( array(
   "name" => esc_html__("OT Services Tab Style 1", 'modis'),
   "base" => "servicest1",
   "class" => "",
   "icon" => "icon-st",
   "category" => 'Modis Elements',
   "params" => array(       
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__('Number show', 'modis'),
         "param_name" => "number",
      ),
      array(
         "type" => "checkbox",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__('Use Description', 'modis'),
         "param_name" => "desc",
      ), 
    )));
}

// Service Tab Style 2
if(function_exists('vc_map')){
   vc_map( array(
   "name" => esc_html__("OT Services Tab Style 2", 'modis'),
   "base" => "servicest",
   "class" => "",
   "icon" => "icon-st",
   "category" => 'Modis Elements',
   "params" => array(       
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__('Number show', 'modis'),
         "param_name" => "number",
      ),  
      array(
         "type" => "checkbox",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__('Use Feature Image In Content', 'modis'),
         "param_name" => "use",
      ), 
      array(
         "type" => "checkbox",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__('Use Description', 'modis'),
         "param_name" => "desc",
      ), 
    )));
}

// Our Facts
if(function_exists('vc_map')){
   vc_map( array(
   "name" => esc_html__("OT Our Facts", 'modis'),
   "base" => "facts",
   "class" => "",
   "icon" => "icon-st",
   "category" => 'Modis Elements',
   "params" => array( 
      array(
         "type" => "dropdown",
         "heading" => esc_html__('Fun Facts Style', 'modis'),
         "param_name" => "style",
         "value" => array(
                     esc_html__('Style 1 : Fun Facts With Number', 'modis')    => 'style1',
                     esc_html__('Style 2 : Fun Facts With Content', 'modis')   => 'style2', 
                     ), 
      ),
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__('Icon', 'modis'),
         "param_name" => "icon",
         "description" => wp_kses("Enter icon ex: icon_clock_alt . Find icon <a href='http://vegatheme.com/html/archi-icons-etlinefont/'>here</a>", 'modis'),
      ),  
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__('Title', 'modis'),
         "param_name" => "title",
      ),     
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__('Number', 'modis'),
         "param_name" => "number",
         "dependency"  => array( 'element' => 'style', 'value' => array( 'style1') ),
      ), 
      array(
         "type" => "textarea_html",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__('Content', 'modis'),
         "param_name" => "content",
         "dependency"  => array( 'element' => 'style', 'value' => array( 'style2') ),
      ),
      array(
         "type" => "dropdown",
         "heading" => esc_html__('Icon Align', 'modis'),
         "param_name" => "align",
         "value" => array(
                     esc_html__('Left', 'modis')    => 'left',
                     esc_html__('Right', 'modis')   => 'right', 
                     ), 
         "dependency"  => array( 'element' => 'style', 'value' => array( 'style2') ),
      ),
      array(
         "type" => "dropdown",
         "heading" => esc_html__('Width Of Columns Icon', 'modis'),
         "param_name" => "width",
         "value" => array(
                     esc_html__('5 Columns', 'modis')    => '5',
                     esc_html__('6 Columns', 'modis')   => '6', 
                     esc_html__('7 Columns', 'modis')   => '7', 
                     ), 
         "dependency"  => array( 'element' => 'style', 'value' => array( 'style2') ),
      ),
    )));
}

// Testimonial Slider
if(function_exists('vc_map')){
   vc_map( array(
   "name" => esc_html__("OT Testimonial Slider", 'modis'),
   "base" => "testis",
   "class" => "",
   "icon" => "icon-st",
   "category" => 'Modis Elements',
   "params" => array(
      array(
         "type" => "dropdown",
         "heading" => esc_html__('Style Show', 'modis'),
         "param_name" => "style",
         "value" => array(
                     esc_html__('Style 1 : Small Text', 'modis')    => 'style1',
                     esc_html__('Style 2 : Big Text', 'modis')   => 'style2', 
                     ), 
      ),
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__('Number Show', 'modis'),
         "param_name" => "number",
      ), 
    )));
}

// Text Auto Write
if(function_exists('vc_map')){
   vc_map( array(
   "name" => esc_html__("OT Text Auto Write", 'modis'),
   "base" => "write",
   "class" => "",
   "icon" => "icon-st",
   "category" => 'Modis Elements',
   "params" => array(  
      array(
         "type" => "dropdown",
         "heading" => esc_html__('Font Size', 'modis'),
         "param_name" => "size",
         "value" => array(
                     esc_html__('Normal', 'modis')  => 'normal',
                     esc_html__('Small', 'modis')   => 'small', 
                     esc_html__('Big', 'modis')     => 'big',
                     ), 
      ),
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__('Title', 'modis'),
         "param_name" => "title",
      ), 
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__('Text Auto Write 1', 'modis'),
         "param_name" => "auto1",
      ), 
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__('Text Auto Write 2', 'modis'),
         "param_name" => "auto2",
      ),
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__('Text Auto Write 3', 'modis'),
         "param_name" => "auto3",
      ),
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__('Text Auto Write 4', 'modis'),
         "param_name" => "auto4",
      ),
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__('Text Auto Write 5', 'modis'),
         "param_name" => "auto5",
      ),
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__('Text Auto Write 6', 'modis'),
         "param_name" => "auto6",
      ),     
    )));
}

// Lastest New
if(function_exists('vc_map')){
   vc_map( array(
   "name" => esc_html__("OT Lastest New", 'modis'),
   "base" => "lastestnew",
   "class" => "",
   "icon" => "icon-st",
   "category" => 'Modis Elements',
   "params" => array(  
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__('Number Show', 'modis'),
         "param_name" => "number",
      ),
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__('Excerpt', 'modis'),
         "param_name" => "excerpt",
      ),
    )));
}

// Home Video Popup
if(function_exists('vc_map')){
   vc_map( array(
   "name" => esc_html__("OT Home Video Popup", 'modis'),
   "base" => "homevideo",
   "class" => "",
   "icon" => "icon-st",
   "category" => 'Modis Elements',
   "params" => array(  
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__('Video Link', 'modis'),
         "param_name" => "link",
      ),
    )));
}

// Home Video Background
if(function_exists('vc_map')){
   vc_map( array(
   "name" => esc_html__("OT Home Video Background", 'modis'),
   "base" => "homevideob",
   "class" => "",
   "icon" => "icon-st",
   "category" => 'Modis Elements',
   "params" => array( 
      array(
         "type" => "dropdown",
         "heading" => esc_html__('Style', 'modis'),
         "param_name" => "style",
         "value" => array(
                     esc_html__('Style Salon', 'modis')     => 'style1',
                     esc_html__('Style Barber', 'modis')     => 'style2', 
                     ), 
      ), 
      array(
         "type" => "dropdown",
         "heading" => esc_html__('Video Type', 'modis'),
         "param_name" => "type",
         "value" => array(
                     esc_html__('Video HTML5', 'modis')     => 'type1',
                     esc_html__('Video Youtube, Vimeo', 'modis')     => 'type2', 
                     ), 
      ),
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__('Video Link mp4', 'modis'),
         "param_name" => "mp4",
         "dependency"  => array( 'element' => 'type', 'value' => array( 'type1') ),
      ),
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__('Video Link Webm', 'modis'),
         "param_name" => "webm",
         "dependency"  => array( 'element' => 'type', 'value' => array( 'type1') ),
      ),
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__('Video Link', 'modis'),
         "param_name" => "link",
         "dependency"  => array( 'element' => 'type', 'value' => array( 'type2') ),
      ),
      array(
         "type" => "attach_image",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Video Poster", 'modis'),
         "param_name" => "photo",
         "value" => "",
      ),
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__('Title', 'modis'),
         "param_name" => "title",
      ),
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__('Subtitle', 'modis'),
         "param_name" => "subtitle",
      ),
      array(
        'type' => 'vc_link',
         "heading" => esc_html__("Button", 'modis'),
         "param_name" => "linkbox",       
         "description" => esc_html__("Add link to Button.", 'modis'),
      ),
    )));
}

// Home Image Scale
if(function_exists('vc_map')){
   vc_map( array(
   "name" => esc_html__("OT Home Image Scale", 'modis'),
   "base" => "homeimgscale",
   "class" => "",
   "icon" => "icon-st",
   "category" => 'Modis Elements',
   "params" => array(       
      array(
         "type" => "attach_image",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Image Background", 'modis'),
         "param_name" => "photo",
         "value" => "",
      ),
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__('Title', 'modis'),
         "param_name" => "title",
      ),
      array(
        'type' => 'vc_link',
         "heading" => esc_html__("Button", 'modis'),
         "param_name" => "linkbox",       
         "description" => esc_html__("Add link to Button.", 'modis'),
      ),
    )));
}

// Process
if(function_exists('vc_map')){
   vc_map( array(
   "name" => esc_html__("OT Process", 'modis'),
   "base" => "process",
   "class" => "",
   "icon" => "icon-st",
   "category" => 'Modis Elements',
   "params" => array(  
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__('Number', 'modis'),
         "param_name" => "number",
      ),
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__('Title', 'modis'),
         "param_name" => "title",
      ),
      array(
         "type" => "textarea_html",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__('Content', 'modis'),
         "param_name" => "content",
      ),
    )));
}


?>