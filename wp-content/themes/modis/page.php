<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package modis
 */
get_header(); 
$sub = get_post_meta(get_the_ID(),'_cmb_page_sub', true);
?>

    <!-- subheader -->
    <section id="subheader" class="subh-center" data-stellar-background-ratio=".2"
        <?php if( function_exists( 'rwmb_meta' ) ) { ?>       
            <?php $images = rwmb_meta( '_cmb_subheader_image', "type=image_advanced&size=full" ); ?>
            <?php if($images){ foreach ( $images as $image ) { ?>
            <?php $img =  $image['full_url']; ?>
              style="background-image: url('<?php echo esc_url($img); ?>');"
            <?php } } ?>
        <?php } ?>
    >
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1><?php the_title(); ?></h1>
                    <?php if($sub != ''){ ?><h4><?php echo esc_attr($sub); ?></h4><?php } ?>
                </div>
            </div>
        </div>
    </section>
    <!-- subheader close -->

    <div id="content">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <?php while (have_posts()) : the_post(); ?>
                        <?php the_post_thumbnail() ?>
                        <article> 
                            <?php the_content(); ?>
                        </article>
                        <?php
                            if ( comments_open() || get_comments_number() ) :
                                comments_template();
                            endif;
                        ?>      
                    <?php endwhile; ?>
                    <div class="text-center">
                        <?php echo modis_pagination(); ?>    
                    </div>
                </div>
                <div class="col-md-4">
                    <?php get_sidebar();?>  
                </div>
            </div>
        </div>
        <?php
            wp_link_pages( array(
                'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'modis' ) . '</span>',
                'after'       => '</div>',
                'link_before' => '<span>',
                'link_after'  => '</span>',
                'pagelink'    => '<span class="screen-reader-text">' . esc_html__( 'Page', 'modis' ) . ' </span>%',
                'separator'   => '<span class="screen-reader-text">, </span>',
            ) );
        ?>
    </div>

<?php get_footer(); ?>
