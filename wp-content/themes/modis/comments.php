<?php
/**
 * The template for displaying comments.
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package modis
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>

<div id="comments" class="comments-area">
	

	<?php if ( have_comments() ) : ?>
		<div class="comments-wrapper">		
			
		    <ul class="comment-list">
					<?php wp_list_comments('callback=modis_theme_comment'); ?>
				<?php
					// Are there comments to navigate through?
					if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) :
				?>
					<nav class="navigation comment-navigation" role="navigation">		   
						<div class="nav-previous"><?php previous_comments_link( wp_kses( '&larr; Older Comments', 'modis' ) ); ?></div>
						<div class="nav-next"><?php next_comments_link( wp_kses( 'Newer Comments &rarr;', 'modis' ) ); ?></div>
		                <div class="clearfix"></div>
					</nav><!-- .comment-navigation -->
				<?php endif; // Check for comment navigation ?>

				<?php if ( ! comments_open() && get_comments_number() ) : ?>
					<p class="no-comments"><?php esc_html_e( 'Comments are closed.' , 'modis' ); ?></p>
				<?php endif; ?>	
		    </ul>
		</div>		
	<?php endif; ?>	

	<?php
    	if ( is_singular() ) wp_enqueue_script( "comment-reply" );
		$aria_req = ( $req ? " aria-required='true'" : '' );
        $comment_args = array(
                'id_form' => 'reply-form',                                
                'title_reply'=> wp_kses('ADD YOUR COMMENT', 'modis'),
                'fields' => apply_filters( 'comment_form_default_fields', array(
                    'author' => '<input id="author" name="author" id="name" class="form-control" type="text" value="" placeholder="'. wp_kses( 'your name...', 'modis' ) .'" />',
                    'email' => '<input id="email" name="email" id="name" class="form-control" type="text" value="" placeholder="'. wp_kses( 'your email...', 'modis' ) .'" />',                    
                ) ),                                
                 'comment_field' => '<textarea rows="7" name="comment" '.$aria_req.' id="comment-message" class="form-control" placeholder="'. wp_kses( 'your message...', 'modis' ) .'" ></textarea>',                                                   
                 'label_submit' => wp_kses( 'send message', 'modis' ),
                 'comment_notes_before' => '',
                 'comment_notes_after' => '',   
                 'class_submit'      => 'btn btn-primary btn-comment',            
	        )
	    ?>
	    <?php comment_form($comment_args); ?>

</div>	

<!-- #comments -->
