<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package modis
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="">
<?php global $modis_option; ?>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

    <!-- Favicons
    ================================================== -->
    <?php if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) { ?>
        <!-- Favicons
        ================================================== -->
        <?php if(!empty($modis_option['favicon']['url'])){ ?>
            <link rel="shortcut icon" href="<?php echo esc_url($modis_option['favicon']['url']); ?>" type="image/png">
        <?php } ?>
        <?php if(!empty($modis_option['apple_icon']['url'])){ ?>
            <link rel="apple-touch-icon" href="<?php echo esc_url($modis_option['apple_icon']['url']); ?>">
        <?php } ?>
        <?php if(!empty($modis_option['apple_icon_72']['url'])){ ?>
            <link rel="apple-touch-icon" sizes="72x72" href="<?php echo esc_url($modis_option['apple_icon_72']['url']); ?>">
        <?php } ?>
        <?php if(!empty($modis_option['apple_icon_114']['url'])){ ?>
            <link rel="apple-touch-icon" sizes="114x114" href="<?php echo esc_url($modis_option['apple_icon_114']['url']); ?>">
        <?php } ?>
    <?php } ?>

<?php wp_head(); ?>
    
</head>

<body <?php body_class(); ?>>    

   <div id="wrapper">
        <?php 
        if(isset($modis_option['header_layout']) and $modis_option['header_layout']=="layout2" ){
                get_template_part('framework/headers/header-2');
        }else{ ?>
        <header class="header_center">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div id="logo">
                            <a href="<?php echo esc_url( home_url('/') ); ?>">
                                <img class="logo logo_dark_bg" src="<?php echo esc_url($modis_option['logo']['url']); ?>" alt="">
                                <img class="logo logo_light_bg" src="<?php echo esc_url($modis_option['logo-scroll']['url']); ?>" alt="">
                            </a>
                        </div>
                        <!-- small button begin -->
                        <div id="menu-btn"></div>
                        <!-- small button close -->

                        <!-- mainmenu begin -->
                        <nav>
                            <?php
                                $left = array(
                                    'theme_location'  => 'left',
                                    'menu'            => '',
                                    'container'       => '',
                                    'container_class' => '',
                                    'container_id'    => '',
                                    'menu_class'      => '',
                                    'menu_id'         => '',
                                    'echo'            => true,
                                    'fallback_cb'     => 'wp_bootstrap_navwalker::fallback',
                                    'walker'          => new wp_bootstrap_navwalker(),
                                    'before'          => '',
                                    'after'           => '',
                                    'link_before'     => '',
                                    'link_after'      => '',
                                    'items_wrap'      => '<ul id="mainmenu" class="menu-left">%3$s</ul>',
                                    'depth'           => 0,
                                );
                                if ( has_nav_menu( 'left' ) ) {
                                    wp_nav_menu( $left );
                                }
                            ?>

                            <ul id="mainmenu" class="logo-center">
                                <li class="logo_pos">
                                    <a class="logo" href="<?php echo esc_url( home_url('/') ); ?>">
                                        <img class="c_logo_light" src="<?php echo esc_url($modis_option['logo']['url']); ?>" style="margin-top: -15px;">
                                        <img class="c_logo_dark" src="<?php echo esc_url($modis_option['logo-scroll']['url']); ?>" style="margin-top: -15px;">
                                    </a>
                                </li>
                            </ul>

                            <?php
                                $right = array(
                                    'theme_location'  => 'right',
                                    'menu'            => '',
                                    'container'       => '',
                                    'container_class' => '',
                                    'container_id'    => '',
                                    'menu_class'      => '',
                                    'menu_id'         => '',
                                    'echo'            => true,
                                    'fallback_cb'     => 'wp_bootstrap_navwalker::fallback',
                                    'walker'          => new wp_bootstrap_navwalker(),
                                    'before'          => '',
                                    'after'           => '',
                                    'link_before'     => '',
                                    'link_after'      => '',
                                    'items_wrap'      => '<ul id="mainmenu" class="menu-right">%3$s</ul>',
                                    'depth'           => 0,
                                );
                                if ( has_nav_menu( 'right' ) ) {
                                    wp_nav_menu( $right );
                                }
                            ?>  
                        </nav>

                        <div class="clearfix"></div>
                    </div>
                    <!-- mainmenu close -->

                </div>
            </div>
        </header>
        <!-- header close -->  
        <?php } ?>