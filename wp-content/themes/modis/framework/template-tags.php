<?php
/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package modis
 */

if ( ! function_exists( 'modis_entry_meta' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function modis_entry_meta() {
	$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
	if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
		$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><!--<time class="updated" datetime="%3$s">%4$s</time>-->';
	}

	$time_string = sprintf( $time_string,
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() ),
		esc_attr( get_the_modified_date( 'c' ) ),
		esc_html( get_the_modified_date() )
	);

	$posted_on = sprintf(
		esc_html_x( '%s', 'post date', 'modis' ),
		//'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
        $time_string . '<span class="separator">|</span>'
	);

    echo '<span class="posted-on">' . $posted_on . '</span>'; // WPCS: XSS OK.

    $format = get_post_format();
    switch ($format) {
        case $format == 'video':
            echo "<i class='fa fa-film'></i>";
            break;
        case $format == 'audio':
            echo "<i class='fa fa-music'></i>";
            break;
        case $format == 'gallery':
            echo "<i class='fa fa-picture-o'></i>";
            break;      
        case $format == 'quote':
            echo "<i class='fa fa-quote-right'></i>";
            break;
        case $format == 'image':
            echo "<i class='fa fa-picture-o'></i>";
            break;                                   
        default:
           echo "<i class='fa fa-pencil'></i>";
    }

	$byline = sprintf(
		esc_html_x( 'By: %s', 'post author', 'modis' ),
		'<a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a>'
	);

    echo '<span class="separator">|</span><span class="byline"> ' . $byline . '</span>'; // WPCS: XSS OK.

}
endif;

if ( ! function_exists( 'modis_excerpt_length' ) ) :
/**** Change length of the excerpt ****/
function modis_excerpt_length() {
      global $modis_option;
      if(isset($modis_option['blog_excerpt'])){
        $limit = $modis_option['blog_excerpt'];
      }else{
        $limit = 15;
      }  
      $excerpt = explode(' ', get_the_excerpt(), $limit);

      if (count($excerpt)>=$limit) {
        array_pop($excerpt);
        $excerpt = implode(" ",$excerpt).'...';
      } else {
        $excerpt = implode(" ",$excerpt);
      } 
      $excerpt = preg_replace('`[[^]]*]`','',$excerpt);
      return $excerpt;
}
endif;

if ( ! function_exists( 'modis_excerpt' ) ) :
/** Excerpt Section Blog Post **/
function modis_excerpt($limit) {
  $excerpt = explode(' ', get_the_excerpt(), $limit);
  if (count($excerpt)>=$limit) {
    array_pop($excerpt);
    $excerpt = implode(" ",$excerpt).'...';
  } else {
    $excerpt = implode(" ",$excerpt);
  }
  $excerpt = preg_replace('`[[^]]*]`','',$excerpt);
  return $excerpt;
}
endif;

if ( ! function_exists( 'image_size_theme_setup' ) ) :
add_action( 'after_setup_theme', 'image_size_theme_setup' );
function image_size_theme_setup() {
    add_image_size( 'category-thumb', 300 ); // 300 pixels wide (and unlimited height)
    add_image_size( 'homepage-thumb', 700, 500, array( 'left', 'top' ) ); // (cropped)
}
endif;

if ( ! function_exists( 'modis_tag_cloud_widget' ) ) :
/**custom function tag widgets**/
function modis_tag_cloud_widget($args) {
    $args['number'] = 0; //adding a 0 will display all tags
    $args['largest'] = 18; //largest tag
    $args['smallest'] = 14; //smallest tag
    $args['unit'] = 'px'; //tag font unit
    $args['format'] = 'list'; //ul with a class of wp-tag-cloud
    $args['exclude'] = array(20, 80, 92); //exclude tags by ID
    return $args;
}
add_filter( 'widget_tag_cloud_args', 'modis_tag_cloud_widget' );
endif;

/** Excerpt Section Blog Post **/
function modis_blog_excerpt($limit) {
  $excerpt = explode(' ', get_the_excerpt(), $limit);
  if (count($excerpt)>=$limit) {
    array_pop($excerpt);
    $excerpt = implode(" ",$excerpt).'...';
  } else {
    $excerpt = implode(" ",$excerpt);
  }
  $excerpt = preg_replace('`[[^]]*]`','',$excerpt);
  return $excerpt;
}

if ( ! function_exists( 'modis_pagination' ) ) :
//pagination
function modis_pagination($prev = 'PREV', $next = 'NEXT', $pages='') {
    global $wp_query, $wp_rewrite;
    $wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;
    if($pages==''){
        global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
    }
    $pagination = array(
        'base'          => str_replace( 999999999, '%#%', get_pagenum_link( 999999999 ) ),
        'format'        => '',
        'current'       => max( 1, get_query_var('paged') ),
        'total'         => $pages,
        'prev_text'     => $prev,
        'next_text'     => $next,       
        'type'          => 'list',
        'end_size'      => 3,
        'mid_size'      => 3
);
    $return =  paginate_links( $pagination );
    echo str_replace( "<ul class='page-numbers'>", '<ul class="pagination">', $return );
}
endif;

if ( ! function_exists( 'modis_custom_wp_admin_style' ) ) :
function modis_custom_wp_admin_style() {

        wp_register_style( 'modis_custom_wp_admin_css', get_template_directory_uri() . '/framework/admin/admin-style.css', false, '1.0.0' );
        wp_enqueue_style( 'modis_custom_wp_admin_css' );

        wp_enqueue_script( 'modis-backend-js', get_template_directory_uri()."/framework/admin/admin-script.js", array( 'jquery' ), '1.0.0', true );
        wp_enqueue_script( 'modis-backend-js' );
}
add_action( 'admin_enqueue_scripts', 'modis_custom_wp_admin_style' );
endif;

if ( ! function_exists( 'modis_search_form' ) ) :
/* Custom form search */
function modis_search_form( $form ) {
    $form = '<form role="search" method="get" action="' . esc_url(home_url( '/' )) . '" class="searchform" >  
        <input type="search" id="search" class="search-field" value="' . get_search_query() . '" name="s" placeholder="'.__('type to search&hellip;', 'modis').'" />
        <button id="btn-search" type="submit"></button>
        <div class="clearfix"></div>
    </form>';
    return $form;
}
add_filter( 'get_search_form', 'modis_search_form' );
endif;

/* Custom comment List: */
function modis_theme_comment($comment, $args, $depth) {    
   $GLOBALS['comment'] = $comment; ?>
   <li class="post-content-comment grey-section">
      <div class="img">
    <?php echo get_avatar($comment,$size='80',$default='http://0.gravatar.com/avatar/ad516503a11cd5ca435acc9bb6523536?s=70' ); ?>
    </div>
    <div class="comment-content">
      <h6><?php printf(__('%s','modis'), get_comment_author()) ?></h6>
    </div>    
    <div class="date">
      <span class="c_date"><?php the_time('dS M Y'); ?></span>
            <span class="c_reply"><?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?></span>
    </div>
    <div class="comment-content">
    <?php if ($comment->comment_approved == '0'){ ?>
       <p><em><?php _e('Your comment is awaiting moderation.','modis') ?></em></p>
    <?php }else{ ?>
            <?php comment_text() ?>
         <?php } ?>
    </div>    
     <div class="clearfix"></div> 
  </li> 
<?php
}

// Add specific CSS class by filter
add_filter( 'body_class', 'modis_body_class_names' );
function modis_body_class_names( $classes ) {
    global $modis_option;
    $theme = wp_get_theme();

    if(isset($modis_option['preloader_mode']) and $modis_option['preloader_mode']=="preloader_logo"){
        $classes[] = 'royal_preloader';
    }
  
  $classes[] = 'modis-theme-ver-'.$theme->version;
  $classes[] = 'wordpress-version-'.get_bloginfo( 'version' );
    
    // return the $classes array
    return $classes;
}

if(!function_exists('modis_custom_frontend_scripts')){
    function modis_custom_frontend_scripts(){
        global $modis_option; 
    ?>
        <script type="text/javascript">
            window.jQuery = window.$ = jQuery;  
            (function($) { "use strict";                

                <?php if(isset($modis_option['preloader_mode']) and $modis_option['preloader_mode']=="preloader_logo" and $modis_option['preload-switch']==true ){ ?>
                    //Preloader Logo
                    Royal_Preloader.config({
                        mode           : 'logo',
                        logo           : '<?php echo esc_js($modis_option['logo_preload']['url']); ?>',
                        logo_size      : [<?php echo esc_js($modis_option['prelogo_width']); ?>, <?php echo esc_js($modis_option['prelogo_height']); ?>],
                        showProgress   : true,
                        showPercentage : true,
                        text_colour: '<?php echo esc_js($modis_option['preload-text-color']); ?>',
                        background:  '<?php echo esc_js($modis_option['preload-background-color']); ?>'
                    });
                <?php } ?>

            })(jQuery);
        </script>
    <?php        
    }
}
add_action('wp_footer', 'modis_custom_frontend_scripts');