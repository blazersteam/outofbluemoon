<?php

    /**
     * For full documentation, please visit: http://docs.reduxframework.com/
     * For a more extensive sample-config file, you may look at:
     * https://github.com/reduxframework/redux-framework/blob/master/sample/sample-config.php
     */

    if ( ! class_exists( 'Redux' ) ) {
        return;
    }

    // This is your option name where all the Redux data is stored.
    $opt_name = "modis_option";

    /**
     * ---> SET ARGUMENTS
     * All the possible arguments for Redux.
     * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
     * */

    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        'opt_name' => 'modis_option',
        'use_cdn' => TRUE,
        'display_name'     => $theme->get('Name'),
        'display_version'  => $theme->get('Version'),
        'page_title' => 'Modis Options',
        'update_notice' => FALSE,
        'admin_bar' => TRUE,
        'menu_type' => 'menu',
        'menu_title' => 'Modis Options',
        'allow_sub_menu' => TRUE,
        'page_parent_post_type' => 'your_post_type',
        'customizer' => FALSE,
        'dev_mode'   => false,
        'default_mark' => '*',
        'hints' => array(
            'icon_position' => 'right',
            'icon_color' => 'lightgray',
            'icon_size' => 'normal',
            'tip_style' => array(
                'color' => 'light',
            ),
            'tip_position' => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect' => array(
                'show' => array(
                    'duration' => '500',
                    'event' => 'mouseover',
                ),
                'hide' => array(
                    'duration' => '500',
                    'event' => 'mouseleave unfocus',
                ),
            ),
        ),
        'output' => TRUE,
        'output_tag' => TRUE,
        'settings_api' => TRUE,
        'cdn_check_time' => '1440',
        'compiler' => TRUE,
        'page_permissions' => 'manage_options',
        'save_defaults' => TRUE,
        'show_import_export' => TRUE,
        'database' => 'options',
        'transient_time' => '3600',
        'network_sites' => TRUE,
    );    

    Redux::setArgs( $opt_name, $args );

    /*
     * ---> END ARGUMENTS
     */

    /*
     * ---> START HELP TABS
     */

    $tabs = array(
        array(
            'id'      => 'redux-help-tab-1',
            'title'   => esc_html__( 'Theme Information 1', 'modis' ),
            'content' => esc_html__( '<p>This is the tab content, HTML is allowed.</p>', 'modis' )
        ),
        array(
            'id'      => 'redux-help-tab-2',
            'title'   => esc_html__( 'Theme Information 2', 'modis' ),
            'content' => esc_html__( '<p>This is the tab content, HTML is allowed.</p>', 'modis' )
        )
    );
    Redux::setHelpTab( $opt_name, $tabs );

    // Set the help sidebar
    $content = esc_html__( '<p>This is the sidebar content, HTML is allowed.</p>', 'modis' );
    Redux::setHelpSidebar( $opt_name, $content );


    /*
     * <--- END HELP TABS
     */


    /*
     *
     * ---> START SECTIONS
     *
     */

    // ACTUAL DECLARATION OF SECTIONS          
    Redux::setSection( $opt_name, array(
        'icon' => ' el-icon-stackoverflow',
        'title' => esc_html__('Miscellaneous Settings', 'modis'),
        'fields' => array( 
            array(
                'id'       => 'animate-switch',
                'type'     => 'switch', 
                'title'    => esc_html__('Animation?', 'modis'),
                'subtitle' => esc_html__('If you do not want to use animated effects elements when scroll page down for both mobile and desktop, just turn it off.', 'modis'),
                'default'  => true,
            ), 
            array(
                'id' => 'gmap_apikey',
                'type' => 'text',
                'title' => __('Google Map API Key', 'modis'),
                'subtitle' => __('Add your google map api key.', 'modis'),
                'default' => 'AIzaSyAvpnlHRidMIU374bKM5-sx8ruc01OvDjI'
            ), 
            array(
                'id' => 'header_all',
                'type' => 'media',
                'title' => esc_html__('Background Header All page', 'modis'),
                'subtitle' => esc_html__('Background Image', 'modis'),
                'default' => array('url' => get_template_directory_uri().'/images/subheader-4.jpg')
            ),                                                   
        )
    ) );

    Redux::setSection( $opt_name, array(
        'icon' => ' el-icon-repeat',
        'title' => esc_html__('Preload Settings', 'modis'),
        'fields' => array(            
            array(
                'id'       => 'preload-switch',
                'type'     => 'switch', 
                'title'    => esc_html__('Preload Off?', 'modis'),
                'subtitle' => esc_html__('If you do not want to use preload, you can turn it off.', 'modis'),
                'default'  => true,
            ),  
            array(
                'id' => 'preloader_mode',
                'type' => 'select',
                'title' => esc_html__('Preloader Style', 'modis'),
                'subtitle' => esc_html__('Preloader style: preload logo or preload progress', 'modis'),
                'desc' => esc_html__('You can choose one of two preload style, Default: Progress Style.', 'modis'),
                'options'  => array(
                    'preloader_progress' => 'Progress Style',                    
                    'preloader_logo' => 'Logo Style',                                                                 
                ),
                'default' => 'preloader_progress',
            ),             
            array(
                'id' => 'preload-text-color',
                'type' => 'color',
                'title' => esc_html__('Preload Text Color', 'modis'),
                'subtitle' => esc_html__('Pick the preload text color (default: #dddddd).', 'modis'),
                'output' => array('#jprePercentage'),
                'default' => '#dddddd',
                'validate' => 'color',
            ), 
            array(
                'id' => 'preload-background-color',
                'type' => 'color',
                'title' => esc_html__('Preload Background Color', 'modis'),
                'subtitle' => esc_html__('Pick the preload background color (default: #ffffff).', 'modis'),
                'output' => array('#jpreOverlay, .royal_preloader, html'),
                'default' => '#ffffff',
                'validate' => 'color',
            ), 
            array(
                'id' => 'preload-typography',
                'type' => 'typography',
                'output' => array('#royal_preloader.royal_preloader_logo .royal_preloader_percentage, #jprePercentage'),
                'title' => esc_html__('Preloader percentage', 'modis'),
                'subtitle' => esc_html__('Number 100% running', 'modis'),
                'google' => true,
                'letter-spacing' => true, 
                'word-spacing' => true, 
                'text-transform' => true,            
                'units'       =>'px', 
                'color'       => false,
                'default' => array( 
                    'font-style'  => '', 
                    'font-family' => '',
                    'font-size'   => '', 
                    'line-height' => ''
                ),
            ),  
        )
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Logo Style', 'modis' ),
        'id'         => 'preload-logo-style',        
        'subsection' => true,
        'fields'     => array(
            array(
                'id' => 'logo_preload',
                'type' => 'media',
                'url' => false,
                'title' => __('Logo Preload (Logo Style)', 'modis'),
                'compiler' => 'true',
                //'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc' => __('Upload your logo preload', 'modis'),
                'subtitle' => __('Recommended size: 204px & 110px', 'modis'),
                'default' => array('url' => get_template_directory_uri().'/images/logo_light.png'),
            ),
            array(
                'id' => 'prelogo_width',
                'type' => 'text',
                'title' => __('Logo width (Logo Style)', 'modis'),
                'subtitle' => __('Input logo width, default: 204', 'modis'),
                'default' => '204'
            ),  
            array(
                'id' => 'prelogo_height',
                'type' => 'text',
                'title' => __('Logo height (Logo Style)', 'modis'),
                'subtitle' => __('Input logo height, default: 110', 'modis'),
                'default' => '110'
            ),          
        )
    ) );

    Redux::setSection( $opt_name, array(
        'icon' => ' el-icon-picture',
        'title' => esc_html__('Logo & Favicon Settings', 'modis'),
        'fields' => array(
            array(
                'id' => 'favicon',
                'type' => 'media',
                'url' => false,
                'title' => esc_html__('Favicon', 'modis'),
                'compiler' => 'true',
                //'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc' => esc_html__('Upload your favicon.', 'modis'),
                'subtitle' => esc_html__('Favicon format .png', 'modis'),
               'default' => array('url' => get_template_directory_uri().'/images/favicon.png'),                     
            ),
            array(
                'id' => 'logo',
                'type' => 'media',
                'url' => false,
                'title' => esc_html__('Logo Static', 'modis'),
                'compiler' => 'true',
                //'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc' => esc_html__('Upload your logo.', 'modis'),
                'subtitle' => esc_html__('Logo format .png, .jpg', 'modis'),
               'default' => array('url' => get_template_directory_uri().'/images/logo.png'),                     
            ),
            array(
                'id' => 'logo-scroll',
                'type' => 'media',
                'url' => false,
                'title' => esc_html__('Logo Scroll', 'modis'),
                'compiler' => 'true',
                //'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc' => esc_html__('Upload your logo.', 'modis'),
                'subtitle' => esc_html__('Logo format .png, .jpg', 'modis'),
               'default' => array('url' => get_template_directory_uri().'/images/logo_light.png'),                     
            ), 
            array(
                'id' => 'apple_icon',
                'type' => 'media',
                'url' => false,
                'title' => __('Apple Touch Icon 57x57', 'modis'),
                'compiler' => 'true',
                //'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc' => __('Upload your Apple touch icon 57x57.', 'modis'),
                'subtitle' => __('', 'modis'),
                'default' => array('url' => get_template_directory_uri().'/images/apple-touch-icon.png'),
            ),                  
            array(
                'id' => 'apple_icon_72',
                'type' => 'media',
                'url' => false,
                'title' => __('Apple Touch Icon 72x72', 'modis'),
                'compiler' => 'true',
                //'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc' => __('Upload your Apple touch icon 72x72.', 'modis'),
                'subtitle' => __('', 'modis'),
                'default' => array('url' => get_template_directory_uri().'/images/apple-touch-icon-72x72.png'),
            ),
            array(
                'id' => 'apple_icon_114',
                'type' => 'media',
                'url' => false,
                'title' => __('Apple Touch Icon 114x114', 'modis'),
                'compiler' => 'true',
                //'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc' => __('Upload your Apple touch icon 114x114.', 'modis'),
                'subtitle' => __('', 'modis'),
                'default' => array('url' => get_template_directory_uri().'/images/apple-touch-icon-114x114.png'),
            ),                                      
        )
    ) );

    Redux::setSection( $opt_name, array(
        'title' => __( 'Header Setting', 'modis' ),
        'id'    => 'header',
        'icon'  => 'el-icon-qrcode'
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => __( 'Header Layout', 'modis' ),
        'id'         => 'header-layout',        
        'subsection' => true,
        'fields'     => array(
            array(
                'id' => 'header_layout',
                'type' => 'select',
                'title' => esc_html__('Header layout', 'modis'),
                'subtitle' => esc_html__('Header layout', 'modis'),
                'desc' => esc_html__('Header layout : select header layout', 'modis'),
                'options'  => array(
                    'layout1' => 'Header layout 1',
                    'layout2' => 'Header layout 2',
                ),
                'default' => '1',
            ),
        ),
    ) );
    Redux::setSection( $opt_name, array(
        'title'      => __( 'Header Static', 'modis' ),
        'id'         => 'static-header',        
        'subsection' => true,
        'fields'     => array(
            array(
                'id' => 'static-header-background-color',
                'type' => 'color',
                'title' => esc_html__('Static Header Background Color', 'modis'),
                'subtitle' => esc_html__('Pick the static header background color for the theme (default: transparent).', 'modis'),
                'default' => 'transparent',
                'validate' => 'color',
            ),                     
            array(
                'id' => 'static-header-text-color',
                'type' => 'color',
                'title' => esc_html__('Static Header Text Color', 'modis'),
                'subtitle' => esc_html__('Pick the static header text color for the theme (default: #fcfcfc).', 'modis'),
                'default' => '#fcfcfc',
                'validate' => 'color',
            ),
            array(
                'id' => 'static-header-background-submenu-color',
                'type' => 'color',
                'title' => esc_html__('Static Header Submenu Background Color', 'modis'),
                'subtitle' => esc_html__('Pick the static header background color for the theme (default: #333).', 'modis'),
                'default' => '#333',
                'validate' => 'color',
            ), 
        ),
    ) );
    Redux::setSection( $opt_name, array(
        'title'      => __( 'Header Scroll', 'modis' ),
        'id'         => 'scroll-header',        
        'subsection' => true,
        'fields'     => array(
            array(
                'id' => 'scroll-header-background-color',
                'type' => 'color',
                'title' => esc_html__('Scroll Header Background Color', 'modis'),
                'subtitle' => esc_html__('Pick the scroll header background color for the theme (default: #ffffff).', 'modis'),
                'default' => 'rgba(255,255,255,.95)',
                'validate' => 'color',
            ),                          
            array(
                'id' => 'scroll-header-text-color',
                'type' => 'color',
                'title' => esc_html__('Scroll Header Text Color', 'modis'),
                'subtitle' => esc_html__('Pick the scroll header text color for the theme (default: #333).', 'modis'),
                'default' => '#333',
                'validate' => 'color',
            ),
            array(
                'id' => 'scroll-header-background-submenu-color',
                'type' => 'color',
                'title' => esc_html__('Static Header Submenu Background Color', 'modis'),
                'subtitle' => esc_html__('Pick the static header background color for the theme (default: #fff).', 'modis'),
                'default' => '#fff',
                'validate' => 'color',
            ), 
        ),
    ) );
    
    Redux::setSection( $opt_name, array(
        'title' => __( 'Blog Settings', 'modis' ),
        'id'    => 'blog',
        'icon'  => 'el-icon-blogger',
        'fields'     => array(
            array(
                'id' => 'blog_style',
                'type' => 'select',
                'title' => esc_html__('Blog Layout', 'modis'),
                'subtitle' => esc_html__('Select Blog Layout', 'modis'),
                'options'  => array(
                    'style1' => 'Blog Right Sidebar',
                    'style2' => 'Blog Left Sidebar',
                    'style3' => 'Blog Fullwidth',   
                ),
                'default' => 'style1',
            ), 
            array(
                'id' => 'blog_excerpt',
                'type' => 'text',
                'title' => esc_html__('Blog custom excerpt lenght', 'modis'),
                'subtitle' => esc_html__('Input Blog custom excerpt lenght', 'modis'),
                'desc' => esc_html__('Blog custom excerpt lenght', 'modis'),
                'default' => '30'
            ),            
        ),
    ) );

    Redux::setSection( $opt_name, array(
        'icon' => 'el el-shopping-cart-sign',
        'title'      => __( 'Shop Settings', 'modis' ),
        'id'         => 'shop-setting',
        'fields'     => array(
            array(
                'id' => 'shop_thumbnail',
                'type' => 'media',
                'url' => false,
                'title' => esc_html__('Background Header Shop Pages', 'modis'),
                'compiler' => 'true',
                //'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc' => esc_html__('Background Header Shop Pages', 'modis'),
                'subtitle' => esc_html__('Background Header Shop Pages', 'modis'),
               'default' => array('url' => get_template_directory_uri().'/images/subheader-4.jpg'),                     
            ), 
            array(
                'id' => 'shop_title',
                'type' => 'text',
                'title' => esc_html__('Title', 'modis'),
                'subtitle' => esc_html__('Input Shop page title', 'modis'),
                'desc' => esc_html__('Title of shop page', 'modis'),
                'default' => 'Shop'
            ),
            array(
                'id' => 'shop_sub',
                'type' => 'text',
                'title' => esc_html__('Subtitle', 'modis'),
                'subtitle' => esc_html__('Input Shop page subtitle', 'modis'),
                'desc' => esc_html__('Subitle of shop page', 'modis'),
                'default' => 'Always Up To Date'
            ),
        ),
    ) );

    Redux::setSection( $opt_name, array(
        'icon' => 'el-icon-group',
        'title' => esc_html__('Social Settings', 'modis'),
        'fields' => array(
            array(
                'id' => 'facebook',
                'type' => 'text',
                'title' => esc_html__('Facebook Url', 'modis'),
                //'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'default' => '',
            ),
            array(
                'id' => 'twitter',
                'type' => 'text',
                'title' => esc_html__('Twitter Url', 'modis'),
                //'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'default' => '',
            ),
            array(
                'id' => 'google',
                'type' => 'text',
                'title' => esc_html__('Google+ Url', 'modis'),
                //'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'default' => '',
            ),                      
            array(
                'id' => 'github',
                'type' => 'text',
                'title' => esc_html__('Github Url', 'modis'),
                //'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'default' => ''
            ),
            array(
                'id' => 'youtube',
                'type' => 'text',
                'title' => esc_html__('Youtube Url', 'modis'),
                //'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'default' => '',
            ),
            array(
                'id' => 'linkedin',
                'type' => 'text',
                'title' => esc_html__('Linkedin Url', 'modis'),
                //'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'default' => '',
            ),
            array(
                'id' => 'dribbble',
                'type' => 'text',
                'title' => esc_html__('Dribbble Url', 'modis'),
                //'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'default' => '',
            ),
            array(
                'id' => 'behance',
                'type' => 'text',
                'title' => esc_html__('Behance Url', 'modis'),
                //'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'default' => ''
            ),
            array(
                'id' => 'instagram',
                'type' => 'text',
                'title' => esc_html__('Instagram Url', 'modis'),
                //'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'default' => ''
            ),
            array(
                'id' => 'skype',
                'type' => 'text',
                'title' => esc_html__('Skype Url', 'modis'),
                //'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'default' => ''
            ),  
            array(
                'id' => 'pinterest',
                'type' => 'text',
                'title' => esc_html__('pinterest Url', 'modis'),
                //'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'default' => ''
            ), 
            array(
                'id' => 'vimeo',
                'type' => 'text',
                'title' => esc_html__('vimeo Url', 'modis'),
                //'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'default' => ''
            ), 
            array(
                'id' => 'tumblr',
                'type' => 'text',
                'title' => esc_html__('tumblr Url', 'modis'),
                //'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'default' => ''
            ), 
            array(
                'id' => 'soundcloud',
                'type' => 'text',
                'title' => esc_html__('soundcloud Url', 'modis'),
                //'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'default' => ''
            ), 
            array(
                'id' => 'lastfm',
                'type' => 'text',
                'title' => esc_html__('lastfm Url', 'modis'),
                //'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'default' => ''
            ), 
            array(
                'id' => 'rss',
                'type' => 'text',
                'title' => esc_html__('RSS Url', 'modis'),
                //'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'default' => '',
            ),  
            array(
                'id' => 'email',
                'type' => 'text',
                'title' => esc_html__('Email Address', 'modis'),
                //'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'default' => '',
            ), 
            array(
                'id' => 'social_extend',
                'type' => 'editor',
                'title' => __('Add your social extend', 'modis'),
                'subtitle' => __('Add your social html code here, if your social network not have on list social above.', 'modis'),
                'description' => esc_html__('HTML code: <a target="_blank" href="#"><i class="fa fa-facebook"></i></a> , find more icons: http://fontawesome.io/icons/#brand', 'modis'),
                'default' => '',
            ),
        )
    ) );

    Redux::setSection( $opt_name, array(
        'title' => __( 'Footer Setting', 'modis' ),
        'id'    => 'footer',
        'icon'  => 'el-icon-credit-card',
        'fields'     => array(              
            array(
                'id' => 'footer_text',
                'type' => 'editor',
                'title' => esc_html__('Footer Text', 'modis'),
                'subtitle' => esc_html__('Copyright Text', 'modis'),
                'default' => 'Copyright 2019 - Modis by OceanThemes',
            ),
        ),
    ) );  

    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Footer Styling', 'modis' ),        
        'id'         => 'footer-styling',
        'subsection' => true,
        'fields'     => array(
            array(
                'id' => 'title-footer-color',
                'type' => 'color',
                'title' => esc_html__('Footer Title Siderbar Color', 'modis'),
                'subtitle' => esc_html__('Pick the bottom footer background color for the theme (default: #fff).', 'modis'),
                'output'      => array('color' => 'footer h3, footerh4, footerh5, footer h6, .de_light footer h3'),
                'default' => '#fff',
                'validate' => 'color',
            ),
            array(
                'id' => 'sidebar-footer-background-color',
                'type' => 'color',
                'title' => esc_html__('Main Footer Background Color', 'modis'),
                'subtitle' => esc_html__('Pick the sidebar footer background color for the theme (default: #111).', 'modis'),
                'output'      => array('background-color' => 'footer'),
                'default' => '#111',
                'validate' => 'color',
            ),            
            array(
                'id' => 'bot-footer-color',
                'type' => 'color',
                'title' => esc_html__('Main Footer Text Color', 'modis'),
                'subtitle' => esc_html__('Pick the bottom footer background color for the theme (default: #ccc).', 'modis'),
                'output'      => array('color' => 'footer, footer a:visited, footer a'),
                'default' => '#ccc',
                'validate' => 'color',
            ),            
            array(
                'id' => 'bot-footer-background-color',
                'type' => 'color',
                'title' => esc_html__('Bottom Footer Background Color', 'modis'),
                'subtitle' => esc_html__('Pick the bottom footer background color for the theme (default: #151515).', 'modis'),
                'output'      => array('background-color' => '.subfooter'),
                'default' => '#151515',
                'validate' => 'color',
            ),                     
            array(
                'id' => 'footer_bottom_textcolor',
                'type' => 'color',
                'title' => esc_html__('Bottom Footer Text Color', 'modis'),
                'subtitle' => esc_html__('Pick the Footer Bottom text color for the theme.', 'modis'),
                'output'      => array('color' => '.subfooter'),
                'default' => '',
                'validate' => 'color',
            ),  
        )
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Footer Spacing', 'modis' ),
        'id'         => 'footer-design-spacing',        
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'foorer-spacing',
                'type'     => 'spacing',
                'output'   => array( 'footer .main-footer' ),
                // An array of CSS selectors to apply this font style to
                'mode'     => 'padding',
                // absolute, padding, margin, defaults to padding
                'all'      => false,
                // Have one field that applies to all
                //'top'           => false,     // Disable the top
                'right'         => false,     // Disable the right
                //'bottom'        => false,     // Disable the bottom
                'left'          => false,     // Disable the left
                'units'          => array( 'em', 'px', '%' ),      // You can specify a unit value. Possible: px, em, %
                'units_extended'=> 'true',    // Allow users to select any type of unit
                //'display_units' => 'false',   // Set to false to hide the units if the units are specified
                'title'    => esc_html__( 'Padding Main Footer', 'modis' ),
                'subtitle' => esc_html__( 'Allow your users to choose the spacing or margin they want.', 'modis' ),
                'desc'     => esc_html__( 'You can enable or disable any piece of this field. Top, Right, Bottom, Left, or Units.', 'modis' ),
                'default'  => array(
                    'padding-top'    => '',
                    'padding-bottom' => '',
                )
            ),
            array(
                'id'             => 'foorer-bottom-spacing',
                'type'           => 'spacing',
                'output'   => array( '.subfooter' ),
                // An array of CSS selectors to apply this font style to
                'mode'           => 'padding',
                // absolute, padding, margin, defaults to padding
                'all'            => false,
                // Have one field that applies to all
                //'top'           => false,     // Disable the top
                'right'         => false,     // Disable the right
                //'bottom'        => false,     // Disable the bottom
                'left'          => false,     // Disable the left
                'units'          => array( 'em', 'px', '%' ),      // You can specify a unit value. Possible: px, em, %
                'units_extended' => 'true',    // Allow users to select any type of unit
                //'display_units' => 'false',   // Set to false to hide the units if the units are specified
                'title'          => esc_html__( 'Padding Footer Bottom', 'modis' ),
                'subtitle'       => esc_html__( 'Allow your users to choose the spacing or margin they want.', 'modis' ),
                'desc'           => esc_html__( 'You can enable or disable any piece of this field. Top, Right, Bottom, Left, or Units.', 'modis' ),
                'default'        => array(
                    'padding-top'    => '',
                    'padding-bottom' => '',
                )
            ),
        )
    ) );      
   
    Redux::setSection( $opt_name, array(
        'icon' => ' el-icon-hourglass',
        'id'    => 'cms',
        'title' => esc_html__('Coming Soon Settings', 'modis'),
        
    ) );
    Redux::setSection( $opt_name, array(
        'title'      => __( 'General', 'modis' ),
        'id'         => 'cms_general',        
        'subsection' => true,
        'fields' => array(              
            array(
                'id' => 'cms_logo',
                'type' => 'media',
                'title' => esc_html__('Logo', 'modis'),
                'subtitle' => esc_html__('Logo in page coming soon', 'modis'),
                'desc' => esc_html__('Use For Coming Soon Page', 'modis'),
                'default' => array('url' => get_template_directory_uri().'/images/logo.png')
            ),
            array(
                'id' => 'cms_title',
                'type' => 'text',
                'title' => esc_html__('Title', 'modis'),
                'subtitle' => esc_html__('Input coming soon title', 'modis'),
                'default' => 'Our Website Coming Soon'
            ),
            array(
                'id' => 'cms_subtitle',
                'type' => 'text',
                'title' => esc_html__('Subtitle', 'modis'),
                'subtitle' => esc_html__('Input coming soon subtitle', 'modis'),
                'default' => 'Time left until launching'
            ),
            array(
                'id'          => 'cms_date',
                'type'        => 'date',
                'title'       => esc_html__('Date Option', 'modis'), 
                'subtitle'    => esc_html__('No validation can be done on this field type', 'modis'),
                'desc'        => esc_html__('This is the description field, again good for additional info.', 'modis'),
                'placeholder' => 'Click to enter a date',
                'default' => '12/23/2018'
            ),
            array(
                'id'       => 'cms_hide',
                'type'     => 'switch', 
                'title'    => esc_html__('Use Coming Soon Hide Content', 'modis'),
                'subtitle' => esc_html__('Look, it\'s on!', 'modis'),
                'default'  => true,
            ),                   
        ),   
    ) );
    Redux::setSection( $opt_name, array(
        'title'      => __( 'Coming Soon Image', 'modis' ),
        'id'         => 'cms_image',        
        'subsection' => true,
        'fields' => array(              
            array(
                'id' => 'cms_bg',
                'type' => 'media',
                'title' => esc_html__('Background Image', 'modis'),
                'subtitle' => esc_html__('Background Image', 'modis'),
                'desc' => esc_html__('Use For Coming Soon Page', 'modis'),
                'default' => array('url' => get_template_directory_uri().'/images/bg-cms.jpg')
            ),          
        ),   
    ) );
    Redux::setSection( $opt_name, array(
        'title'      => __( 'Coming Soon Video', 'modis' ),
        'id'         => 'cms_video',        
        'subsection' => true,
        'fields' => array(              
            array(
                'id' => 'cms_bg_video',
                'type' => 'media',
                'title' => esc_html__('Video Poster', 'modis'),
                'subtitle' => esc_html__('Background Image Video', 'modis'),
                'default' => array('url' => get_template_directory_uri().'/images/video-poster.jpg')
            ),
            array(
                'id' => 'mp4',
                'type' => 'text',
                'title' => esc_html__('Link Video Mp4', 'modis'),
                'subtitle' => esc_html__('Input Link video HTML5 (.mp4)', 'modis'),
                'default' => 'http://themenesia.com/video/salon_1.mp4'
            ), 
            array(
                'id' => 'webm',
                'type' => 'text',
                'title' => esc_html__('Link Video Webm', 'modis'),
                'subtitle' => esc_html__('Input Link video HTML5 (.webm)', 'modis'),
                'default' => 'http://themenesia.com/video/salon_1.webm'
            ), 
        ),   
    ) );


    Redux::setSection( $opt_name, array(
        'title' => __( 'Styling Options', 'modis' ),
        'id'    => 'styling',
        'icon'  => 'el-icon-website'
    ) );
    
    Redux::setSection( $opt_name, array(
        'title'      => __( 'Theme Color', 'modis' ),
        'id'         => 'theme_color',        
        'subsection' => true,
        'fields'     => array(
            array(
                'id' => 'theme_version',
                'type' => 'select',
                'title' => esc_html__('Theme Version', 'modis'),
                'desc' => esc_html__('Theme Version : select version of theme (salon or barber)', 'modis'),
                'options'  => array(
                    'ver1' => 'Version Salon',
                    'ver2' => 'Version Barber',
                ),
                'default' => 'ver1',
            ),
            array(
                'id' => 'main-color',
                'type' => 'color',
                'title' => esc_html__('Theme Main Color', 'modis'),
                'subtitle' => esc_html__('Pick the main color for the theme (default: #de3370).', 'modis'),
                'default' => '#de3370',
                'validate' => 'color',
            ), 
            array(
                'id' => 'body-font2',
                'type' => 'typography',
                'output' => array('body'),
                'title' => esc_html__('Body Font', 'modis'),
                'subtitle' => esc_html__('Specify the body font properties.', 'modis'),
                'google' => true,
                'font-backup' => true,
                'word-spacing' => true,
                'letter-spacing' => true,
                'text-transform' => true,
                'default' => array(
                    'color' => '',
                    'font-size' => '',
                    'line-height' => '',
                    'font-family' => '',
                    'font-weight' => ''
                ),
            ),            
        ),
    ) );
    Redux::setSection( $opt_name, array(
        'title'      => __( 'Add CSS Code', 'modis' ),
        'id'         => 'code_css',        
        'subsection' => true,
        'fields'     => array(
            array(
                'id' => 'custom-css',
                'type' => 'ace_editor',
                'title' => esc_html__('CSS Code', 'modis'),
                'subtitle' => esc_html__('Paste your CSS code here.', 'modis'),
                'mode' => 'css',
                'theme' => 'monokai',
                'desc' => 'Possible modes can be found at http://ace.c9.io/.',
                'default' => "#header{\nmargin: 0 auto;\n}"
            ),
        ),
    ) );

    /*
     * <--- END SECTIONS
     */
