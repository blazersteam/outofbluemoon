<?php global $modis_option; ?>
<header class="header_left">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <!-- logo begin -->
                <div id="logo">
                    <a href="<?php echo esc_url( home_url('/') ); ?>">
                        <img class="logo logo_dark_bg" src="<?php echo esc_url($modis_option['logo']['url']); ?>" alt="">
                        <img class="logo logo_light_bg" src="<?php echo esc_url($modis_option['logo-scroll']['url']); ?>" alt="">
                    </a>
                </div>
                <!-- logo close -->

                <!-- small button begin -->
                <div id="menu-btn"></div>
                <!-- small button close -->

                <!-- mainmenu begin -->
                <nav>               

                    <?php
                        $primary = array(
                            'theme_location'  => 'primary',
                            'menu'            => '',
                            'container'       => '',
                            'container_class' => '',
                            'container_id'    => '',
                            'menu_class'      => '',
                            'menu_id'         => '',
                            'echo'            => true,
                            'fallback_cb'     => 'wp_bootstrap_navwalker::fallback',
                            'walker'          => new wp_bootstrap_navwalker(),
                            'before'          => '',
                            'after'           => '',
                            'link_before'     => '',
                            'link_after'      => '',
                            'items_wrap'      => '<ul id="mainmenu" class="primary">%3$s</ul>',
                            'depth'           => 0,
                        );
                        if ( has_nav_menu( 'primary' ) ) {
                            wp_nav_menu( $primary );
                        }
                    ?>  
                </nav>

                <div class="clearfix"></div>
            </div>
            <!-- mainmenu close -->

        </div>
    </div>
</header>
<!-- header close -->  