<?php

/**
 * Register meta boxes
 *
 * @since 1.0
 *
 * @param array $meta_boxes
 *
 * @return array
 */

function modis_register_meta_boxes( $meta_boxes ) {

	$prefix = '_cmb_';
	$meta_boxes[] = array(

		'id'       => 'format_detail',

		'title'    => esc_html__( 'Format Details', 'modis' ),

		'pages'    => array( 'post' ),

		'context'  => 'normal',

		'priority' => 'high',

		'autosave' => true,

		'fields'   => array(

			array(

				'name'             => esc_html__( 'Image', 'modis' ),

				'id'               => $prefix . 'image',

				'type'             => 'image_advanced',

				'class'            => 'image',

				'max_file_uploads' => 1,

			),

			array(

				'name'  => esc_html__( 'Gallery', 'modis' ),

				'id'    => $prefix . 'images',

				'type'  => 'image_advanced',

				'class' => 'gallery',

			),			

			array(

				'name'  => esc_html__( 'Audio', 'modis' ),

				'id'    => $prefix . 'link_audio',

				'type'  => 'oembed',

				'cols'  => 20,

				'rows'  => 2,

				'class' => 'audio',

				'desc' => 'Ex: https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/139083759',

			),

			array(

				'name'  => esc_html__( 'Video', 'modis' ),

				'id'    => $prefix . 'link_video',

				'type'  => 'oembed',

				'cols'  => 20,

				'rows'  => 2,

				'class' => 'video',

				'desc' => 'Example: <b>http://www.youtube.com/watch?v=pbZzfZQQuro</b>',

			),			

		),

	);

	$meta_boxes[] = array(
		'id'       => 'post_settings',
		'title'    => esc_html__( 'Single Setting', 'modis' ),
		'pages'    => array(  'post' ),
		'context'  => 'normal',
		'priority' => 'high',
		'autosave' => true,
		'fields'   => array(			
			array(
                'name' => 'Add Title Post',
                'desc' => 'Enter title for single page, leave a blank do not show.',
                'id'   => $prefix . 'post_title',
                'type' => 'text',
            ),	                     		
		),

	);

	$meta_boxes[] = array(
		'id'       => 'page_settings',
		'title'    => esc_html__( 'Page Settings', 'modis' ),
		'pages'    => array( 'page', 'post' ,'ot_post_service' ),
		'context'  => 'normal',
		'priority' => 'high',
		'autosave' => true,
		'fields'   => array(	
			array(
				'name'             => esc_html__( 'Upload background image for top detail page.', 'modis' ),
				'desc' 			   => 'if not upload image, it is use image default for all page setup in theme option.',
				'id'               => $prefix . 'subheader_image',
				'type'             => 'image_advanced',			
				'max_file_uploads' => 1,
			),
            array(
                'name' => 'Add Subtitle Page',
                'desc' => 'Enter subtitle for detail page, leave a blank do not show.',
                'id'   => $prefix . 'page_sub',
                'type' => 'textarea',
            ),            		
		),

	);
	$meta_boxes[] = array(
		'id'         => 'job_testimonial',
		'title'      => 'Testimonials Details',
		'pages'      => array( 'testimonial' ), // Post type
		'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true, // Show field names on the left
		//'show_on'    => array( 'key' => 'id', 'value' => array( 2, ), ), // Specific post IDs to display this metabox
		'fields' => array(
			array(
                'name' => 'Job',
                'desc' => 'Job of Person',
                'id'   => $prefix . 'job_testi',
                'type' => 'text',
            ),
		)
	);	

	$meta_boxes[] = array(
		'id'         => 'service_setting',
		'title'      => 'Service Details',
		'pages'      => array( 'ot_post_service' ), // Post type
		'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true, // Show field names on the left
		//'show_on'    => array( 'key' => 'id', 'value' => array( 2, ), ), // Specific post IDs to display this metabox
		'fields' => array(
			array(
                'name' => 'Content 1',
                'desc' => 'Add service content 1.',
                'id'   => $prefix . 'ser_content1',
                'type' => 'WYSIWYG',
            ), 
            array(
                'name' => 'Content 2',
                'desc' => 'Add service content 2.',
                'id'   => $prefix . 'ser_content2',
                'type' => 'WYSIWYG',
            ), 
		)
	);	

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'modis_register_meta_boxes' );

