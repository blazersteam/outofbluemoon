<?php

/**
 * Enqueues front-end CSS for color scheme.
 *
 * @since Modis 1.0
 *
 * @see wp_add_inline_style()
 */

function modis_color_scheme_css() {
	global $modis_option; 
	wp_add_inline_style( 'modis-style', modis_get_color_scheme_css() );
}
add_action( 'wp_enqueue_scripts', 'modis_color_scheme_css' );



/**
 * Returns CSS for the color schemes.
 *
 * @since Twenty Fifteen 1.0
 *
 * @param array $colors Color scheme colors.
 * @return string Color scheme CSS.
 */

function modis_get_color_scheme_css() {

	global $modis_option;

	$css = <<<CSS

	

	.bg-color,

	#mainmenu li li a:hover,

	.price-row,

	.blog-list .date,

	.blog-read .date,

	.slider-info .text1,

	#filters a.selected,

	.btn-primary,

	.bg-id-color,

	.pagination > .active > a,

	.pagination > .active > span,

	.pagination > .active > a:hover,

	.pagination > .active > span:hover,

	.pagination > .active > a:focus,

	.pagination > .active > span:focus,

	.dropcap,

	.fullwidthbanner-container a.btn,

	.feature-box-big-icon i,

	#testimonial-full,

	.icon-deco i,

	.feature-box-small-icon .border,

	.small-border,

	#jpreBar,

	.date-post,

	.team-list .small-border,

	.de-team-list .small-border,

	.btn-line:hover,a.btn-line:hover,

	.btn-line.hover,a.btn-line.hover,

	.owl-arrow span,

	.de-progress .progress-bar,

	#btn-close-x:hover,

	.box-fx .info,

	.de_tab.tab_steps .de_nav li span,

	#services-list li.active,

	#services-list li a:hover,

	section.bg-color

	.btn-more,

	.widget .small-border,

	.product img:hover,

	#btn-search,

	span.overlay.plus,

	.sub-item-service .c1 span.disc,

	.services-mas ul li .c1 span.disc,

	.h-line,

	.btn-slider,

	.box-icon i,

	.table-set .table.package .c2,

	header.smaller.header_light #mainmenu li li a:hover,

	.bg-color-2,

	.h-line,

	.btn-slider,

	section.bg-color-2,

	.small-border,

	.tiny-border span,

	.table-set div:nth-child(2n+1) .table.package .c2,

	.products li .btn,

	.arrow-up, .arrow-down,

	.custom-show:after,.custom-close:after,

	.blog-list .date-box .day,

	.pagination li span.current,

	.table.package .c2,.woocommerce a.button:hover,

	.woocommerce a.button.alt,.woocommerce input.button,

	.woocommerce input.button.alt,.woocommerce #respond input#submit,

	.woocommerce button.button.alt,a.added_to_cart.wc-forward:hover,

	#back-to-top,
	#mainmenu.menu-left > li li.active > a, #mainmenu.menu-right > li li.active > a,#mainmenu.primary > li li.active > a,
	#mainmenu.menu-left > li li.current-menu-ancestor > a, #mainmenu.menu-right > li li.current-menu-ancestor > a,#mainmenu.primary > li li.current-menu-ancestor > a,
	.border-xs,
	.smaller #mainmenu.menu-left > li li.active > a, .smaller #mainmenu.menu-right > li li.active > a,.smaller #mainmenu.primary > li li.active > a,
	.smaller #mainmenu.menu-left > li li.current-menu-ancestor > a, .smaller #mainmenu.menu-right > li li.current-menu-ancestor > a,.smaller 
	#mainmenu.primary > li li.current-menu-ancestor > a

	{

	background-color:{$modis_option['main-color']}  ;

	}



	.pagination li span.current:hover{

		border: 1px solid {$modis_option['main-color']};

	}



	/* force background color */

	input.btn-custom{

	background-color:{$modis_option['main-color']}   !important;

	}



	/* fore color */

	a,

	.feature-box i,

	#mainmenu li:hover > ul,

	.date-box .day,

	.slider_text h1,

	.id-color,

	.pricing-box li h1,

	.title span,

	i.large:hover,

	.feature-box-small-icon-2 i,

	address span i,

	.pricing-dark .pricing-box li.price-row,

	.price,

	#mainmenu a:hover,

	#mainmenu a.active,

	#mainmenu li a:after,

	header.smaller #mainmenu a.active,

	.pricing-dark .pricing-box li.price-row,

	.dark .feature-box-small-icon i,

	a.btn-slider:after,

	.feature-box-small-icon i,

	a.btn-line:after,

	.team-list .social a,

	.de_contact_info i,

	.de_count,

	.dark .btn-line:hover:after, .dark a.btn-line:hover:after, .dark a.btn-line.hover:after,

	a.btn-text:after,

	.separator span  i,

	.de_tab.tab_steps .de_nav li span:hover,

	.de_testi_by,

	.pf_text,

	.widget_tags li a,

	.dark .btn-line:after, .dark  a.btn-line:after,

	.crumb a,

	#mainmenu > li:hover > a,

	#mainmenu li  div a:hover,

	.de_light  .de_tab.tab_style_1 .de_nav li.active span,

	#mainmenu li ul li a:hover,

	.de_light .team-member .social i:hover,

	a.btn-main-color,.woocommerce .star-rating span,

	.woocommerce-info:before,.woocommerce div.product p.price, .woocommerce div.product span.price,
	#mainmenu.menu-left > li.active > a, #mainmenu.menu-right > li.active > a,#mainmenu.primary > li.active > a,
	#mainmenu.menu-left > li.current-menu-ancestor > a, #mainmenu.menu-right > li.current-menu-ancestor > a,#mainmenu.primary > li.current-menu-ancestor > a,
	#mainmenu.menu-left > li > a:hover, #mainmenu.menu-right > li > a:hover, #mainmenu > li > ul > li  a:hover,
	.main-color,
	.smaller #mainmenu.menu-left > li.active > a, .smaller #mainmenu.menu-right > li.active > a,.smaller #mainmenu.primary > li.active > a,
	.smaller #mainmenu.menu-left > li.current-menu-ancestor > a, .smaller #mainmenu.menu-right > li.current-menu-ancestor > a,.smaller #mainmenu.primary > li.current-menu-ancestor > a,
	.smaller #mainmenu.menu-left > li > a:hover, .smaller #mainmenu.menu-right > li > a:hover, .smaller #mainmenu > li > ul > li  a:hover

	{

	color:{$modis_option['main-color']}   ;

	}



	/* fore color */

	#subheader h4,

	.sub-item-service .c3,

	.services-mas ul li .c3,

	.de_testi blockquote:before

	{

	color:{$modis_option['main-color']}   ;

	}



	/* border color */

	.feature-box i,

	#filters a:hover,

	#filters a.selected,

	.pagination > .active > a,

	.pagination > .active > span,

	.pagination > .active > a:hover,

	.pagination > .active > span:hover,

	.pagination > .active > a:focus,

	.pagination > .active > span:focus

	.feature-box-big-icon i:after,

	.social-icons i,

	.btn-line:hover,a.btn-line:hover,

	.btn-line.hover,a.btn-line.hover,

	.product img:hover,

	.deform-1 input[type=text]:focus,.deform-1 textarea:focus, #search:focus, select:focus,

	.deform-1 .de_light input[type=text]:focus, .deform-1 .de_lighttextarea:focus, .deform-1 .de_light #search:focus,

	.box-outer,

	.box-border.double,

	.selector-img input:checked + img,

	.de_light .form-control.deform-1:focus,

	.form-control.deform-1:focus

	{

	border-color:{$modis_option['main-color']}  ;

	}



	/* border color */

	.box-outer,

	.box-number.square .number

	{

	border-color:{$modis_option['main-color']}   ;

	}



	/* specify element color */

	.box-fx .inner,

	.dark .box-fx .inner,

	.blog-list .date-box .month

	{

	border-bottom-color:{$modis_option['main-color']}   ;

	}



	.de_tab .de_nav li span {

	border-top: 3px solid {$modis_option['main-color']}   ;

	}



	.feature-box-big-icon i:after {

	border-color: {$modis_option['main-color']}    transparent;

	}



	.de_review li.active img{

	border:solid 4px {$modis_option['main-color']}   ;

	}



	.de_light  .de_tab.tab_style_2 .de_nav li.active span {

	border-bottom: solid 6px {$modis_option['main-color']}  ;

	}

	.vc_separator .vc_sep_holder .vc_sep_line{

		border-color: {$modis_option['main-color']}!important;

	}

	.ot-tabs.vc_tta-color-grey.vc_tta-style-classic .vc_tta-tab.vc_active>a{

		border-bottom: solid 3px {$modis_option['main-color']}!important;

	}

	a.btn-main-color{
		border:solid 1px {$modis_option['main-color']};  		
	}

	header.transparent{
		background: {$modis_option['static-header-background-color']};
	}

	#mainmenu.menu-left > li > a, 
	#mainmenu.menu-right > li > a,
	#mainmenu > li > ul > li  a, #mainmenu.primary > li > a{

		color: {$modis_option['static-header-text-color']};

	}

	#mainmenu > li > ul > li  a{
		background: {$modis_option['static-header-background-submenu-color']};
	}
	header.smaller.header_light{
		background: {$modis_option['scroll-header-background-color']};
	}
	header.smaller.header_light #mainmenu a{
		color: {$modis_option['scroll-header-text-color']};
	}
	header.smaller.header_light #mainmenu li li a{
		color: {$modis_option['scroll-header-text-color']};		
	}
	header.smaller.header_light #mainmenu li li a{
		background: {$modis_option['scroll-header-background-submenu-color']};
	}

	#bg-coming-soon-page{
		background:url({$modis_option['cms_bg']['url']});
	}

	#subheader{
		background:url({$modis_option['header_all']['url']});
	}

	.woocommerce-info {
	    border-top-color: {$modis_option['main-color']};
	}
	.de_light section.bg-color{
		background-color:{$modis_option['main-color']}  ;
	}

CSS;
	return $css;
}