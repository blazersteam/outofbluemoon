<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package modis
 */
$subtitle = get_post_meta(get_the_ID(),'_cmb_page_subtitle', true);
$link_audio = get_post_meta(get_the_ID(),'_cmb_link_audio', true);
$link_video = get_post_meta(get_the_ID(),'_cmb_link_video', true);
$title = get_post_meta(get_the_ID(),'_cmb_post_title', true);
$sub = get_post_meta(get_the_ID(),'_cmb_page_sub', true);
get_header(); ?>

<section id="subheader" class="subh-center" data-stellar-background-ratio=".2"
  <?php if( function_exists( 'rwmb_meta' ) ) { ?>       
    <?php $images = rwmb_meta( '_cmb_subheader_image', "type=image_advanced&size=full" ); ?>
    <?php if($images){ foreach ( $images as $image ) { ?>
    <?php $img =  $image['full_url']; ?>
      style="background-image: url('<?php echo esc_url($img); ?>');"
    <?php } } ?>
  <?php } ?>
>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1><?php if($title){echo htmlspecialchars_decode($title);}else{ esc_html_e('Blog Single', 'modis');} ?></h1>
                <h4><?php if($sub){echo htmlspecialchars_decode($sub);}else{ esc_html_e('The Blog Single', 'modis');} ?></h4>
            </div>
        </div>
    </div>
</section>
<!-- CONTENT BLOG -->
<?php while (have_posts()) : the_post(); ?>
  <div id="content">
    <div class="container">
        <div class="row">
            <?php if($modis_option['blog_style']=='style2'){ ?>
                <div class="col-md-4">
                    <?php get_sidebar();?>  
                </div>
            <?php } ?>
            <div class="<?php if($modis_option['blog_style']=='style3'){echo 'col-md-12';}else{echo 'col-md-8';} ?>">
                <ul class="blog-list">
                <li class="single">                  
                  <div class="post-content">
                      <div class="post-image">
                        <?php $format = get_post_format(); ?>
                        <?php if($format=='audio'){ ?>

                          <iframe style="width:100%" src="<?php echo esc_url( $link_audio ); ?>"></iframe>
            
                          <?php } elseif($format=='video'){ ?>

                            <div class="post-thumbnail">
                                <?php echo wp_oembed_get( $link_video ); ?>
                            </div>
           
                          <?php } elseif($format=='gallery'){ ?>

                            <div  id="blog-carousel-<?php the_ID() ?>" class="blog-carousel">
                              <?php if( function_exists( 'rwmb_meta' ) ) { ?>  
                                  <?php $images = rwmb_meta( '_cmb_images', "type=image" ); ?>
                                  <?php if($images){ ?>
                                    
                                      <?php                                                        
                                        foreach ( $images as $image ) {                              
                                      ?>
                                      <?php $img = $image['full_url']; ?>
                                        <div class="item"><img src="<?php echo esc_url($img); ?>" alt=""></div> 
                                      <?php } ?>                   
                                    
                                  <?php } ?>
                                <?php } ?>
                            </div>
                            <script type="text/javascript">
                              (function($){
                                "use strict";                              
                                $(document).ready(function() {
                                    $("#blog-carousel-<?php the_ID() ?>").owlCarousel({
                                      autoPlay: 3000,
                                      items : 1,
                                      singleItem:true,                                    
                                    });
                                  });                              
                              })(this.jQuery);
                            </script>
              
                          <?php } elseif ($format=='image'){ ?>
                          <?php if( function_exists( 'rwmb_meta' ) ) { ?>  
                            <?php $images = rwmb_meta( '_cmb_image', "type=image" ); ?>
                            <?php if($images){ ?>
                            <?php                                                        
                              foreach ( $images as $image ) {                              
                              ?>
                              <?php $img = $image['full_url']; ?>
                              <img src="<?php echo esc_url($img); ?>" alt="">
                              <?php } ?>
                            <?php } ?>
                          <?php } ?>

                          <?php }else{ $format=='stadard' ?>
                              <?php if(get_the_post_thumbnail()){ ?>              
                                  <img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>" alt="">
                              <?php } ?>
                          <?php } ?>

                      </div>
                      <div class="date-box">
                          <div class="day"><?php the_time('d'); ?></div>
                          <div class="month"><?php the_time('M'); ?></div>
                      </div>
                      <div class="post-text page-content">
                        <h3 class="single-title"><?php the_title(); ?></h3>
                         <?php the_content(); ?>
                      </div>
                  </div>
                    
                  <div class="post-info">
                    <span>
                      <i class="fa fa-user"></i><?php _e('By : ','modis') ?><?php the_author_posts_link(); ?>
                    </span> 
                    <?php if(has_tag()) { ?>
                    <span>
                      <i class="fa fa-tag"></i><?php the_tags('', ', ' ); ?>
                    </span> 
                    <?php } ?>
                    <?php if ( comments_open()) : ?>
                      <span>
                        <i class="fa fa-comment"></i><span class="comments_number"><?php comments_number( wp_kses('0 comment', 'modis'), wp_kses('1 comment', 'modis'), wp_kses('% comments', 'modis') ); ?></span>
                      </span> 
                    <?php  endif; ?>               
                  </div>
              </li>
              </ul>
              <?php if ( comments_open()) : ?>
                <div class='comments-box'>
                  <h3><?php comments_number( wp_kses('0 comment', 'modis'), wp_kses('1 comment', 'modis'), wp_kses('% comments', 'modis') ); ?></h3>
                </div>
                <?php comments_template(); ?> 
              <?php  endif; ?>
              
            </div>

            <?php if($modis_option['blog_style']=='style1'){ ?>
              <div class="col-md-4">
                  <?php get_sidebar();?>  
              </div>
            <?php } ?>

        </div>
    </div>
 </div>

<?php endwhile;?>
  <!-- END CONTENT BLOG -->    
    
<?php get_footer(); ?>
