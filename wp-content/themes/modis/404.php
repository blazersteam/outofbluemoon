<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package modis
 */

get_header(); ?>

	<section id="subheader" class="subh-center" data-stellar-background-ratio=".2">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h1><?php esc_html_e('404','modis'); ?></h1>
        </div>
      </div>
    </div>
  </section>

  <div class="site-content">
        <section class="padding-top-bottom-90">
          	<div class="container">
                <div class="row">
                  <div class="col-lg-12 text-center">
                    <p class="font-size-36 font-weight-100 main-color"><?php esc_html_e('Oops. You have encountered an error.','modis'); ?></p>
                    <p class="margin-top-0"><?php esc_html_e('It appears the page your were looking for does not exist. Sorry about that.','modis'); ?></p>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 col-md-offset-3">
                    <div class="text-center page_search">
                        <p><?php esc_html_e('Try to search something...','modis'); ?></p>
                        <?php get_search_form(); ?>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-12 text-center">
                    <p><?php esc_html_e('or','modis'); ?></p>
                    <p><a href="<?php echo esc_url(home_url('/')); ?>" class="btn btn-default"><i class="fa fa-home"></i> <?php esc_html_e('go back to homepage','modis'); ?> </a></p>
                  </div>
                </div>
          	</div>
        </section>
  	</div>
	
<?php get_footer(); ?>
