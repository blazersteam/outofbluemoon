<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     6.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header(); ?>
	
	<?php global $modis_option; ?>
	<?php $sub = get_post_meta(get_the_ID(),'_cmb_page_sub', true); ?>

    <!-- subheader -->
    <section id="subheader" class="subh-center shop-page" data-stellar-background-ratio=".2" <?php if($modis_option['shop_thumbnail'] != ''){ ?> style="background-image: url('<?php echo esc_url($modis_option['shop_thumbnail']['url']); ?>');" <?php } ?>>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1><?php echo wp_kses( $modis_option['shop_title'], wp_kses_allowed_html('post') ); ?></h1>
                    <?php if($modis_option['shop_sub']){ ?><h4><?php echo wp_kses( $modis_option['shop_sub'], wp_kses_allowed_html('post') ); ?></h4><?php } ?>					                
                </div>
            </div>
        </div>
    </section>
    <!-- subheader close -->

			

	<?php
		/**
		 * woocommerce_before_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		do_action( 'woocommerce_before_main_content' );
	?>		
		<div class="col-md-9">
			
				<?php if ( have_posts() ) : ?>
					
						<?php
							/**
							 * woocommerce_before_shop_loop hook
							 *
							 * @hooked woocommerce_result_count - 20
							 * @hooked woocommerce_catalog_ordering - 30
							 */
							do_action( 'woocommerce_before_shop_loop' );
						?>
					
					
						<?php woocommerce_product_loop_start(); ?>

							<?php woocommerce_product_subcategories(); ?>

							<?php while ( have_posts() ) : the_post(); ?>

								<?php wc_get_template_part( 'content', 'product' ); ?>

							<?php endwhile; // end of the loop. ?>

						<?php woocommerce_product_loop_end(); ?>
					
					<?php
						/**
						 * woocommerce_after_shop_loop hook
						 *
						 * @hooked woocommerce_pagination - 10
						 */
						do_action( 'woocommerce_after_shop_loop' );
					?>				
				<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

					<?php wc_get_template( 'loop/no-products-found.php' ); ?>

				<?php endif; ?>

		</div>

		<div class="col-md-3">
			<?php
				/**
				 * woocommerce_sidebar hook
				 *
				 * @hooked woocommerce_get_sidebar - 10
				 */
				do_action( 'woocommerce_sidebar' );
			?>
		</div>

	<?php
		/**
		 * woocommerce_after_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action( 'woocommerce_after_main_content' );
	?>
	

<?php get_footer( 'shop' ); ?>
