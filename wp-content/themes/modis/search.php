<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package modis
 */

global $modis_option;

get_header(); ?>

    <section id="subheader" class="subh-center" data-stellar-background-ratio=".2">
        <div class="container">
            <div class="row">
                <div class="col-md-12">                    
                    <h1><?php printf( wp_kses( 'Search Results for: %s', 'modis' ),  get_search_query() ); ?></h1>
                </div>
            </div>
        </div>
    </section>

    <div id="content">
        <div class="container">
            <div class="row">
                <?php if($modis_option['blog_style']=='style2'){ ?>
                    <div class="col-md-4">
                        <?php get_sidebar();?>  
                    </div>
                <?php } ?>
                <div class="<?php if($modis_option['blog_style']=='style3'){echo 'col-md-12';}else{echo 'col-md-8';} ?>">
                    <ul class="blog-list">
                        <?php if ( have_posts() ) : ?> 
                            <?php 
                                while (have_posts()) : the_post();
                                    get_template_part( 'content', get_post_format() ) ;   // End the loop.
                                endwhile;
                            ?>
                            <!-- Pagination start -->
                            <nav>
                                <?php echo modis_pagination(); ?>    
                            </nav><!-- Pagination end -->
                        <?php else : ?>
                            <div class="no-results not-found">                            
                                <h1><?php esc_html_e( 'Nothing Found', 'modis' ); ?></h1>
                                
                                <div class="page-content text-center">
                                    <p><?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'modis' ); ?></p>
                                    <div class="col-md-8 col-md-offset-2">
                                        <div class="widget_search page_search">
                                            <?php get_search_form(); ?>
                                        </div>
                                    </div>
                                </div><!-- .page-content -->
                            </div><!-- .no-results -->
                        <?php endif; ?>  
                    </ul>                    
                </div>
                <?php if($modis_option['blog_style']=='style1'){ ?>
                    <div class="col-md-4">
                        <?php get_sidebar();?>  
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>    
    
<?php get_footer(); ?>