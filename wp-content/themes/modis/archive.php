<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package modis
 */

global $modis_option;

get_header(); ?>

    <section id="subheader" class="subh-center" data-stellar-background-ratio=".2">
        <div class="container">
            <div class="row">
                <div class="col-md-12">                    
                    <?php
                        the_archive_title( '<h1>', '</h1>' );
                        the_archive_description( '<h4 class="taxonomy-description">', '</h4>' );
                    ?>
                </div>
            </div>
        </div>
    </section>

    <div id="content">
        <div class="container">
            <div class="row">

                <?php if($modis_option['blog_style']=='style2'){ ?>
                    <div class="col-md-4">
                        <?php get_sidebar();?>  
                    </div>
                <?php } ?>

                <div class="<?php if($modis_option['blog_style']=='style3'){echo 'col-md-12';}else{echo 'col-md-8';} ?>">
                    <ul class="blog-list">
                        <?php 
                            $args = array(    
                                'paged' => $paged,
                                'post_type' => 'post',
                            );
                            $wp_query = new WP_Query($args);
                            while ($wp_query -> have_posts()): $wp_query -> the_post();                         
                            get_template_part( 'content', get_post_format() ) ; 
                        ?> 
                        <?php endwhile;?> 
                    </ul>
                    <div class="text-center">
                        <?php echo modis_pagination(); ?>    
                    </div>
                </div>

                <?php if($modis_option['blog_style']=='style1'){ ?>
                    <div class="col-md-4">
                        <?php get_sidebar();?>  
                    </div>
                <?php } ?>

            </div>
        </div>
    </div>    
    
<?php get_footer(); ?>