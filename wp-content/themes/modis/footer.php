<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package modis
 */
global $modis_option; ?>    


    <footer>

        <?php 
            if ( is_active_sidebar( 'footer-area-1' ) 
                || is_active_sidebar( 'footer-area-2' ) 
                || is_active_sidebar( 'footer-area-3' ) 
                || is_active_sidebar( 'footer-area-4' )
            ){ 
        ?>
            <div class="main-footer">
                <div class="container">
                    <div class="row">
                        <?php get_sidebar('footer');?>
                    </div>
                </div>
            </div>        
        <?php } ?>    

        <div class="subfooter">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <?php echo wp_kses( $modis_option['footer_text'], wp_kses_allowed_html('post') ); ?>           
                    </div>
                    <div class="col-md-6 text-right">
                        <div class="social-icons">
                            <?php if($modis_option['facebook']!=''){ ?>                                    
                                <a target="_blank" href="<?php echo esc_url($modis_option['facebook']); ?>"><i class="fa fa-facebook"></i></a>                                  
                            <?php } ?>                                
                            <?php if($modis_option['twitter']!=''){ ?>                                    
                                <a target="_blank" href="<?php echo esc_url($modis_option['twitter']); ?>"><i class="fa fa-twitter"></i></a>                               
                            <?php } ?>                                
                            <?php if($modis_option['google']!=''){ ?>                                    
                                <a target="_blank" href="<?php echo esc_url($modis_option['google']); ?>"><i class="fa fa-google-plus"></i></a>                                
                            <?php } ?>
                            <?php if($modis_option['dribbble']!=''){ ?>
                                <a target="_blank" href="<?php echo esc_url($modis_option['dribbble']); ?>"><i class="fa fa-dribbble"></i></a>
                            <?php } ?>
                            <?php if($modis_option['pinterest']!=''){ ?>
                                <a target="_blank" href="<?php echo esc_url($modis_option['pinterest']); ?>"><i class="fa fa-pinterest"></i></a>
                            <?php } ?>
                            <?php if($modis_option['linkedin']!=''){ ?>
                                <a target="_blank" href="<?php echo esc_url($modis_option['linkedin']); ?>"><i class="fa fa-linkedin"></i></a>
                            <?php } ?>                                
                            <?php if($modis_option['youtube']!=''){ ?>                                    
                                <a target="_blank" href="<?php echo esc_url($modis_option['youtube']); ?>"><i class="fa fa-youtube"></i></a>                                 
                            <?php } ?>  
                            <?php if($modis_option['vimeo']!=''){ ?>
                                <a target="_blank" href="<?php echo esc_url($modis_option['vimeo']); ?>"><i class="fa fa-vimeo-square"></i></a>
                            <?php } ?>
                            <?php if($modis_option['rss']!=''){ ?>                                    
                                <a target="_blank" href="<?php echo esc_url($modis_option['rss']); ?>"><i class="fa fa-rss"></i></a>                                
                            <?php } ?>                                                            
                            <?php if($modis_option['skype']!=''){ ?>
                                <a target="_blank" href="<?php echo esc_url($modis_option['skype']); ?>"><i class="fa fa-skype"></i></a>
                            <?php } ?>                               
                            <?php if($modis_option['instagram']!=''){ ?>
                                <a target="_blank" href="<?php echo esc_url($modis_option['instagram']); ?>"><i class="fa fa-instagram"></i></a>
                            <?php } ?>  
                            <?php if($modis_option['github']!=''){ ?>
                                <a target="_blank" href="<?php echo esc_url($modis_option['github']); ?>"><i class="fa fa-github"></i></a>
                            <?php } ?>
                            <?php if($modis_option['tumblr']!=''){ ?>
                                <a target="_blank" href="<?php echo esc_url($modis_option['tumblr']); ?>"><i class="fa fa-tumblr-square"></i></a>
                            <?php } ?>
                            <?php if($modis_option['soundcloud']!=''){ ?>
                                <a target="_blank" href="<?php echo esc_url($modis_option['soundcloud']); ?>"><i class="fa fa-soundcloud"></i></a>
                            <?php } ?>
                            <?php if($modis_option['behance']!=''){ ?>
                                <a target="_blank" href="<?php echo esc_url($modis_option['behance']); ?>"><i class="fa  fa-behance"></i></a>
                            <?php } ?>
                            <?php if($modis_option['lastfm']!=''){ ?>
                                <a target="_blank" href="<?php echo esc_url($modis_option['lastfm']); ?>"><i class="fa fa-lastfm"></i></a>
                            <?php } ?>  
                            <?php if($modis_option['email']!=''){ ?>
                                <a href="<?php echo esc_url($modis_option['email']); ?>"><i class="fa fa-envelope-o"></i></a>
                            <?php } ?>
                            <?php if($modis_option['social_extend']!=''){ 
                                echo htmlspecialchars_decode(do_shortcode( $modis_option['social_extend'] ));
                            } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </footer>
    <a id="back-to-top" href="#" class="show"></a>
</div>

<?php wp_footer(); ?>

</body>
</html>
