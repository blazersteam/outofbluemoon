<?php
/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache


//Begin Really Simple SSL Load balancing fix
if ((isset($_ENV["HTTPS"]) && ("on" == $_ENV["HTTPS"]))
|| (isset($_SERVER["HTTP_X_FORWARDED_SSL"]) && (strpos($_SERVER["HTTP_X_FORWARDED_SSL"], "1") !== false))
|| (isset($_SERVER["HTTP_X_FORWARDED_SSL"]) && (strpos($_SERVER["HTTP_X_FORWARDED_SSL"], "on") !== false))
|| (isset($_SERVER["HTTP_CF_VISITOR"]) && (strpos($_SERVER["HTTP_CF_VISITOR"], "https") !== false))
|| (isset($_SERVER["HTTP_CLOUDFRONT_FORWARDED_PROTO"]) && (strpos($_SERVER["HTTP_CLOUDFRONT_FORWARDED_PROTO"], "https") !== false))
|| (isset($_SERVER["HTTP_X_FORWARDED_PROTO"]) && (strpos($_SERVER["HTTP_X_FORWARDED_PROTO"], "https") !== false))
|| (isset($_SERVER["HTTP_X_PROTO"]) && (strpos($_SERVER["HTTP_X_PROTO"], "SSL") !== false))
) {
$_SERVER["HTTPS"] = "on";
}
//END Really Simple SSL
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

/** DB Write Permission */
define('FS_METHOD','direct');
define( 'WP_MEMORY_LIMIT', '256M' );
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'outofblu_data' );

/** MySQL database username */
define( 'DB_USER', 'outofblu_master' );

/** MySQL database password */
define( 'DB_PASSWORD', 'qW][@NCUF&Uj' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'R #o=C5nj0isN$PR`#zTJF]mFr{E~)C )H?S<ojG^tSsUKJw-B0W?^}8NBaZV3D ' );
define( 'SECURE_AUTH_KEY',  '6]dY`=)VU_1]fyEfiS>oe>hM$z`z$>m.q6SMY?SQMr9+-8p>+(ovTfZ/9q?mq*pA' );
define( 'LOGGED_IN_KEY',    '@?yVM]}|TqWF;@q8fGQf7ong{;s5z10XOaz.xdy+e1V]IO{$7WhGVjw$9iv!,z]w' );
define( 'NONCE_KEY',        '>1[kt@-F~jS.eEiEO_lOf/gG;F-l&0em);pWUI||*}-MWO-CZmh~{UihNVK1k%oC' );
define( 'AUTH_SALT',        '[CDEw6t[oS[e^64]NKb3Ga~cnC)T=%UO+7.yD3347L ^&L24eD^dCsRaK-):56Lj' );
define( 'SECURE_AUTH_SALT', '}Qr|(Q?tT>o^hVofdSIq%Npq_j}KW{P2Rp8nb{:Z2Gg$1<h=-*]s)wfB(wi3PM}&' );
define( 'LOGGED_IN_SALT',   'p:0k|C}t3[fHBa^o[M7%,!{n&,j4]3;mzR~-#}Vo$ &zLD:f:cLwTKMm+0v~ 4{=' );
define( 'NONCE_SALT',       'KRF3va_KwP}^W065^VaEpOQ_qSA;i4l7{BZ:!_4Y],=b &Djx[D>e~s>XIYILS3-' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'bea_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
